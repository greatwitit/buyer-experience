---
  title: Quickstart for GitLab Continuous Integration 
  description: 'Welcome to the GitLab CI quickstart guide, where you will be guided through the process of setting up a project in GitLab and creating a simple CI configuration in code. This guide will enable you to swiftly initiate your journey with GitLab CI.'
  side_menu:
    anchors:
      text: On this page
      data:
      - text: "Step 1: Create a new Project/Repository"
        href: "#step-1"
        data_ga_name: step-1
        data_ga_location: side-navigation
      - text: "Step 2: Setting the CI Configuration"
        href: "#step-2"
        data_ga_name: step-2
        data_ga_location: side-navigation
      - text: "Step 3 - Committing the change and run the pipeline"
        href: "#step-3"
        data_ga_name: step-3
        data_ga_location: side-navigation
      - text: "Step 4 - View Pipeline run and test results"
        href: "#step-4"
        data_ga_name: step-4
        data_ga_location: side-navigation
      - text: "Step 5 - Fail the pipeline"
        href: "#step-5"
        data_ga_name: step-5
        data_ga_location: side-navigation
      - text: "Step 6 - Implement pipeline-wide failure conditions"
        href: "#step-6"
        data_ga_name: step-6
        data_ga_location: side-navigation
      - text: "Step 7 - Enhance your configuration with built-in pipeline templates"
        href: "#step-7"
        data_ga_name: step-7
        data_ga_location: side-navigation
  hero:
    crumbs:
      - title: Get Started
        href: /get-started/
        data_ga_name: Get Started
        data_ga_location: breadcrumb
      - title: Quickstart for GitLab Continuous Integration 
    header_label: 20 min to complete
    title: Quickstart for GitLab Continuous Integration 
    content: |
      <p>Welcome to the GitLab CI quickstart guide, where you will be guided through the process of setting up a project in GitLab and creating a simple CI configuration in code. This guide will enable you to swiftly initiate your journey with GitLab CI.</p>
      <br>
      <p>The following tasks will be outlined in this quick start guide:</p>
      <br>
      <ul>
      <li>- Establishing a new project.</li>
      <li>- Crafting your initial CI/CD configuration and executing the pipeline.</li>
      <li>- Accessing and reviewing the results of the run.</li>
      <li>- Introducing conditions based on rules to determine job executions.</li>
      <li>- Harnessing the power of Pipeline Templates for seamless integration of beneficial configurations.</li>
      </ul>
  copy_info:
    title: Before you start
    text: |
      Ensure you have a GitLab account. If you don’t have, <a href="https://gitlab.com/users/sign_up" data-ga-name="sign up" data-ga-location="continuous integration page">sign up here</a>.
  steps:
    groups:
      - header: "Step 1: Create a new Project/Repository "
        id: step-1
        items:
          - title: Step overview
            copies:
               - text: |
                  <p>Within a project, various components such as your codebase, CI configuration, planning, analytics, and team members are managed. In this guide, we will create a new project with a blank slate, containing only a readme file.</p> 
          - title: How to
            copies:
              - text: |
                  <ul>
                    <li>Create a new project by clicking the plus icon at the right side of the top bar, and selecting <b>New project/repository</b> </li>
                    <li>Select <b>Create Blank project</b>. Under `Project name` type <b>my-project</b>. </li>
                    <li>Click <b>Create project</b>. </li>
                    <li>Congratulations! you have successfully created your first project. </li>
                  </ul>
      - header: "Step 2: Setting the CI Configuration"
        id: step-2
        items:
          - title: Step overview
            copies:
              - text: |
                   In GitLab, the CI configuration is defined in code using YAML syntax. This configuration specifies instructions to the runner machine regarding job execution, job order, job conditions, and more. To define the CI configuration, you need to create a file called <b>.gitlab-ci.yml</b>, which should be located at the root of your repository. In this guide, we will utilize the webIDE to create and edit this file.
          - title: How to
            copies:
              - title:  Creating the configuration file 
                text: |
                  To access the webIDE, simply click on the WebIDE quick button located in your project. Once inside the webIDE, navigate to the file explorer on the left side. Right-click within the file explorer and choose the <b>New File</b> option. Name the newly created file <b>.gitlab-ci.yml</b>.
              - title: Defining the Pipeline stages
                text: |
                  The order of job executions is determined by the stages defined in the configuration. In this guide, we will define three stages: <b>build, test, and package,</b> in that specific order. Copy and paste the following code into the <b>.gitlab-ci.yml</b> file:
                  <pre>
                  stages:          
                    - build
                    - test
                    - package
                  </pre>
              - title: Defining the pipeline jobs
                text: |
                 Envision a scenario where you are tasked with creating two text files. It is of utmost importance that the concatenation of these files includes the phrase "Hello world." Our objective is to build, <b>test</b>, and <b>package</b> this requirement utilizing pipeline jobs.
              - title: Define a Build-Job
                text: |
                  We will specify a build job that accomplishes the following tasks: creating a text file with the word "Hello," creating another text file with the word "World," and generating a third file that stores the combined content of the two files. We will save the third file as an artifact, so subsequent jobs in test and package stages will be able to access it. 
                  Please insert the provided code below the stages block:
                  <pre>
                  build-job:       
                    stage: build
                    script:
                      - echo "Hello " | tr -d "\n" > file1.txt
                      - echo "world" > file2.txt
                      - cat file1.txt file2.txt > compiled.txt
                    artifacts:
                    paths:
                      - compiled.txt
                  </pre>
              - title: Define a test job
                text: |
                  To validate the integrity of our build, we will incorporate a test job. This job will examine whether the <b>compiled.txt</b> file indeed contains the expected phrase "Hello world". Please insert the following code below the build job:
                  <pre>
                  test:
                    stage: test
                    script: cat compiled.txt | grep -q 'Hello world '
                  </pre>
              - title: Define a package job
                text: |
                  Upon successful completion of the test, our next objective is to generate a package for our code. To accomplish this, we will include a package job. It is important to note that if the test fails, the entire pipeline will be deemed unsuccessful and will not proceed. Please insert the provided code below the test job:
                  <pre>
                  package:
                    stage: package
                    script: cat compiled.txt | gzip > packaged.gz
                    artifacts:
                      paths:
                      - packaged.gz
                  </pre>
              - title: Your complete pipeline should look like this 
                text: |
 
                  <pre>
                  stages:          # List of stages for jobs, and their order of execution
                    - build
                    - test
                    - package 
                    
                  build-job:      
                    stage: build
                    script:
                      - echo "Hello " | tr -d "\n" > file1.txt
                      - echo "world" > file2.txt
                      - cat file1.txt file2.txt > compiled.txt
                    artifacts:
                      paths:
                      - compiled.txt
                    
                  test:
                    stage: test
                    script: cat compiled.txt | grep -q 'Hello world'

                  package:
                    stage: package
                    script: cat compiled.txt | gzip > packaged.gz
                    artifacts:
                      paths:
                      - packaged.gz
                  </pre>
                  <p>Here is a link to the <a href="https://gitlab.com/tech-marketing/ci-quickstart/-/blob/main/.gitlab-ci.yml ">configuration file</a> in our example project.</p>
                  <p>Congratulations!! you built your first CI pipeline. </p>
      - header: "Step 3 - Committing the change and run the pipeline"
        id: step-3
        items:
          - title: Step overview
            copies:
               - text: | 
                   <p>To activate continuous integration (CI) within our project, we must push the <b>.gitlab-ci.yml</b> file to the repository. Once this file is located at the root of the repository, each commit made to the project will automatically initiate a CI pipeline. The initial pipeline will commence immediately after pushing this file to the server.</p>
          - title: How to
            copies:
               - text: |
                  <ul>
                    <li>Click on the Merge icon located to the left of the file explorer.</li>
                    <li>Provide a commit message such as "Adding CI configuration." </li>
                    <li>Click on <b>Commit & Push</b>.</li>
                    <li>When prompted with "Commit to a new branch?" select "No, Use the current branch main".</li>
                    <li>To return to your project, click the <b>Go to project</b> button situated on the bottom left side.</li>
                  </ul>  
                  <br>
                  <p><b>
                  Congratulations! Your project is now successfully configured to     automatically initiate a CI pipeline for every code commit.
                  </b></p>
      - header: Step 4 - View Pipeline run and test results. 
        id: step-4
        items:
          - title: Step Overview 
            copies: 
              - text: |
                  <p>While the pipeline is running, you can monitor the status of it in the <b>CI/CD</b> tab. This feature allows you to easily track the progress of your jobs, including their execution status (such as whether they have started, passed,  failed, etc), as well as any output generated by your job scripts.</p>
          - title: How to
            copies:
              - text: |
                  <ul>
                    <li>Navigate to the GitLab project and locate the left menu.</li>
                    <li>Click on <b>CI/CD</b> in the menu, click <b>Pipelines</b>.</li>
                    <li>On the <b>Pipelines</b> page, locate the pipeline button in the <b>Status</b> column. Click on it to open the pipeline graph.</li>
                    <li>Now, you can observe the jobs and their respective statuses within the pipeline graph.</li>
                    <li>To explore a specific job, click on it to open the job console. This console displays all the steps executed on the Runner machine.</li>
                    <li>Open the package job console to view the steps that were processed by the runner.</li>
                    <li>The package job generates an artifact, you can download it by clicking the <b>download</b> button located on the right side.</li>
                    <li>By following these steps, you can effectively track the pipeline status, inspect job details, and retrieve any relevant artifacts or packages produced during the pipeline execution.</li>
                  </ul>  
                  <br>
                  <p><b>
                  Congratulations on successfully running your first pipeline. The pipeline succeeded! You have now viewed the results and downloaded the job artifact.
                  </b></p>
      - header: Step 5 - Fail the pipeline 
        id: step-5
        items:
          - title: Step overview
            copies:
              - text: |
                  <p>We will change the expected value in the test job, the test job will fail as well as the entire pipeline will fail. </p>
          - title: How to
            copies:
              - text: |
                  <ul>
                    <li>Edit the <b>test</b> job by modifying the phrase "Hello World" to "hello world" (with lowercase letters).</li>
                    <li>Commit the code changes and proceed to view the pipeline, similar to Step 4.</li>
                    <li>Upon inspecting the pipeline, you will observe that the test job has failed. Additionally, the subsequent <b>package</b> job did not start, and the pipeline itself failed as expected.</li>
                  </ul>
      - header: Step 6 - Implement pipeline-wide failure conditions
        id: step-6
        items:
          - title: Step overview
            copies:
              - text: | 
                  <p>In step 5 we saw that job failure failed the entire pipeline. You can introduce logic into your pipeline that determines when a job failure will cause the entire pipeline to fail with the following steps:</p>
                  <ul>
                    <li>Assess the conditions under which you want a job failure to result in pipeline failure. For example, you may want to enforce pipeline failure if a job fails on the main or default branch, while allowing job failures on other branches to proceed with the pipeline.</li>
                    <li>Define rules that govern the failure behavior. You can leverage variables such as $CI_COMMIT_BRANCH to check the current branch and make decisions based on it.</li>
                    <li>Set the appropriate conditions and specify whether the job should be marked as <b>allow_failure: false</b> or <b>allow_failure: true</b>.</li>
                  </ul>
          - title: How to
            copies:
              - text: |
                  <p>How to</p>
                  <ul>
                    <li>Add rules/if conditions to your test job.</li>
                    <li>Use the <b>allow_failure</b> keyword set to <b>true</b> or <b>false</b> based on the branch.</li>
                  </ul>
                  <pre>
                  test:
                    stage: test
                    script: cat compiled.txt | grep -q 'Hello world'
                    rules: 
                      - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
                        allow_failure: false 
                      - if: $CI_COMMIT_BRANCH 
                        allow_failure: true 
                  </pre>
      - header: Step 7 - Enhance your configuration with built-in pipeline templates
        id: step-7
        items:
          - title: Step overview
            copies:
              - text: |
                  <p>To streamline the pipeline configuration setup, you can leverage the built-in pipeline templates provided by GitLab. These templates offer pre-defined configurations for common use cases, such as security scans, aws deployments, etc.</p>
                  <p>Follow these steps to utilize the built-in pipeline templates:</p>
                  <ul>
                    <li>Explore the available pipeline templates offered by GitLab for various scenarios such as building, testing, deploying, and more. These templates can be found <a href="https://gitlab.com/gitlab-org/gitlab-foss/tree/master/lib/gitlab/ci/templates">here</a>.</li>
                    <li>Select the template that aligns with your requirements. </li>
                    <li>Incorporate the template into your pipeline configuration by referencing it in your <b>.gitlab-ci.yml</b> file. You can typically do this by importing the template using the <b>include</b> keyword and specifying the path or URL to the template file.</li>
                  </ul>
                  <p>
                    In this guide we will add Code Quality scan to our configuration using the Code-Quality template. 
                  </p>
          - title: How to
            copies:
              - text: |
                  <ul>
                    <li>Include the code quality template to your <b>.gitlab-ci.yml</b> by adding this code below the stages block.</li>
                      <pre>
                      include:
                        - template: Jobs/Code-Quality.gitlab-ci.yml  #
                      </pre>
                    </li>
                    <li>Commit and push this change. </li>
                  </ul>
                  <p>
                    You will notice that a Code quality job was added to your pipeline. The Code Quality scanner will thoroughly analyze any code changes committed to this repository, and provide valuable feedback, highlighting any code quality issues that require attention and improvement. This valuable insight allows you to enhance the overall quality of your codebase and optimize its performance.
                  </p>
                  <p>
                    That's it! With these steps, you should be able to get started with GitLab CI and automate your project's build and testing processes.
                  </p>
  next_steps:
    header: Next steps
    cards:
      - title: Want to learn more about GitLab CI?
        text: Get a quick introduction to GitLab CI in this informative video. Perfect for beginners and those looking to enhance their understanding of GitLab CI. 
        avatar: /nuxt-images/icons/avatar_orange.png
        col_size: 4
        link:
          text: CI overview demo
          url: "https://youtu.be/WKR-7clknsA"
          data_ga_name: ci overview demo
          data_ga_location: body
      - title: Utilize Issues
        text: GitLab Issues are used to track and manage tasks, bugs, or feature requests within a project. They provide a centralized place for collaboration to discuss, assign, and track the progress of work items.
        avatar: /nuxt-images/icons/avatar_pink.png
        col_size: 4
        link:
          text: Issues
          url: "https://docs.gitlab.com/ee/user/project/issues/"
          data_ga_name: issues
          data_ga_location: body
      - title: Add security scans to your pipeline
        text: With GitLab's integrated security scans, you can easily incorporate them into your CI configuration. This ensures that developers receive instant feedback regarding any potential risks in their code changes.
        avatar: /nuxt-images/icons/avatar_blue.png
        col_size: 4
        link:
          text: Application security
          url: /blog/2023/03/15/getting-started-with-gitlab-application-security/
          data_ga_name: application security
          data_ga_location: body
