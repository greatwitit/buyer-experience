---
  title: 中小企業向けのスタートガイド
  description: 競合他社よりも迅速に市場に投入し、イノベーションを起こす必要があります。 このガイドでは、中小企業に最適なソリューションであるGitLabの使用を開始する方法を説明します。
  side_menu:
    anchors:
      text: このページの目次
      data:
      - text: はじめに
        href: "#getting-started"
        data_ga_name: getting-started
        data_ga_location: side-navigation
      - text: セットアップ
        href: "#get-setup"
        data_ga_name: getting-setup
        data_ga_location: side-navigation
      - text: GitLab を使用する
        href: "#utiliser-gitlab"
        data_ga_name: using-gitlab
        data_ga_location: side-navigation
  hero:
    crumbs:
      - title: 今すぐ始める
        href: /get-started/
        data_ga_name: Get Started
        data_ga_location: breadcrumb
      - title: 中小企業向けのスタートガイド
    header_label: 完了まで 20 分
    title: 中小企業向けのスタートガイド
    content: |
      "競合他社よりも迅速に市場に投入し、イノベーションを起こす必要があります。 複雑なDevSecOpsプロセスによって速度が低下するわけにはいきません。 このガイドは、Ultimate レベルでのセキュリティ、コンプライアンス、プロジェクト計画を含むオプションを使用して、Premium レベルでの自動ソフトウェア開発と配信の基本をすばやく設定するのに役立ちます。"
  copy_info:
    title: 始める前に
    text: |
      GitLab 15.1 (2022 年 6 月 22 日) 以降では、Free レベルの GitLab.com の名前空間は、 <a href="https://docs.gitlab.com/ee/user/namespace/" data-ga-name="namespaces" data-ga-location="body">名前空間</a>ごとに 5 つのメンバーに制限されます。 この制限は、最上位のグループと個人用名前空間に適用されます。 ユーザー数が多い場合は、有料レベルから始めることをお勧めします。
  steps:
    groups:
      - header: はじめに
        show: すべて表示
        hide: すべて隠す
        id: getting-started
        items:
          - title: どのサブスクリプションが適切かを判断する
            copies:
              - title: GitLab SaaS または GitLab セルフマネージド
                text: |
                  <p>GitLab プラットフォームを GitLab に管理させたいですか、それとも自分で管理したいですか?</p>
                link:
                  href: https://docs.gitlab.com/ee/subscriptions/#choose-between-gitlab-saas-or-gitlab-self-managed
                  text: 違いを評価する
                  ga_name: GitLab Saas vs Self-Managed
                  ga_location: body
          - title: どのレベルがニーズを満たすかを判断する
            copies:
              - title: 無料、プレミアム、またはアルティメット
                text: |
                  <p>どのレベルが適切かを判断するには、次の点を考慮してください。 </p>

              - title: ユーザー数
                text: |
                  GitLabサブスクリプションは、SaaSと自己管理の両方に対して同時(シート)モデルを使用します。 ユーザー/シートの数は、レベルの選択に影響を与える可能性があります。 ユーザーが 5 人を超える場合は、有料レベル (プレミアムまたはアルティメット) が必要です。
              - title: 必要なストレージの量
                text: |
                  GitLab SaaS の無料利用枠の名前空間には、5 GB のストレージ制限があります。
              - title: 必要なセキュリティとコンプライアンス
                text: |
                  <p>*シークレット検出、SAST、およびコンテナスキャンは、無料とプレミアムで利用できます。</p>
                  <p>* DAST、依存関係、クラスター イメージ、IaC、API、ファジングなどの追加のスキャナー <a href="https://docs.gitlab.com/ee/user/application_security/"> は、Ultimateで利用できます。</a></p>
                  <p>*マージリクエストパイプラインとセキュリティダッシュボードに統合された実用的な調査結果には、脆弱性管理のためのUltimateが必要です。</p>
                  <p>* コンプライアンス パイプラインには Ultimate が必要です。</p>
                  <p></a> * 当社の<a href="https://docs.gitlab.com/ee/user/application_security/">セキュリティスキャナ</a>および当社の<a href="https://docs.gitlab.com/ee/administration/compliance.html">コンプライアンス機能</a>についてお読みください。"
                link:
                  href: /pricing/
                  text: 詳細については、価格をご覧ください
                  ga_name: pricing
                  ga_location: body
      - header: セットアップ
        show: すべて表示
        hide: すべて隠す
        id: get-setup
        items:
          - title: SaaS サブスクリプション アカウントを設定する
            copies:
              - title: 必要なシート数を決定する
                text: |
                  GitLab SaaSサブスクリプションは、同時(シート)モデルを使用します。 課金期間中のユーザーの最大数に応じてサブスクリプションの料金を支払います。 任意の時点での合計ユーザーがサブスクリプション数を超えない限り、サブスクリプション期間中にユーザーを追加および削除できます。
                link:
                  href: https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#how-seat-usage-is-determined
                  text: シートの使用状況の決定方法の詳細
                  ga_name: Determine how many seats you want
                  ga_location: body
              - title: SaaS サブスクリプションを取得する
                text: |
                  <p>GitLab SaaSは、GitLabのサービスとしてのソフトウェア製品であり、GitLab.com で入手できます。GitLab SaaSを使用するために何もインストールする必要はなく、サインアップするだけで済みます。サブスクリプションによって、プライベート プロジェクトで使用できる機能が決まります。価格ページに移動し、[プレミアムを購入] または [アルティメットを購入] を選択します。<p/>
                  <p>パブリックオープンソースプロジェクトを持つ組織は、オープンソースプログラム用GitLabに積極的に応募できます。50,000ユニットのコンピューティングを含む <a href="/pricing/ultimate">GitLab Ultimate</a>の機能は、オープンソースプログラム用 <a href="/solutions/open-source">GitLabを通じてオープンソース</a> プロジェクトを認定するために無料です。</p>
                link:
                  href: https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#obtain-a-gitlab-saas-subscription
                  text: SaaS サブスクリプションの詳細情報
                  ga_name: Obtain your SaaS subscription
                  ga_location: body
              - title: 必要な CI/CD 共有ランナーの分数を決定する
                text: |
                  <a href="https://docs.gitlab.com/ee/ci/runners/runners_scope.html#shared-runners" data-ga-name="Shared Runners" data-ga-location="body">共有ランナー</a>は、GitLabインスタンス内のすべてのプロジェクトとグループと共有されます。ジョブが共有ランナーで実行される場合、コンピューティング単位が使用されます。 GitLab.com では、コンピューティング ユニットのクォータが <a href="https://docs.gitlab.com/ee/user/namespace/" data-ga-name="namespaces" data-ga-location="body">名前空間</a>ごとに設定され、 <a href="/pricing" data-ga-name="Your license tier" data-ga-location="body">ライセンス レベルによって決まります。</a><p/>
                  <p>毎月のクォータに加えて、GitLab.com では、必要に応じて <a href="https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#purchase-additional-cicd-minutes\" data-ga-name=\"purchase additional compute minutes\" data-ga-location=\"body\">追加のコンピューティング ユニットを購入できます</a>。</p>

          - title: 自己管理型サブスクリプション アカウントを設定する
            copies:
              - title: 必要なシート数を決定する
                text: |
                  GitLab セルフマネージドサブスクリプションは、同時 (シート) モデルを使用します。 課金期間中のユーザーの最大数に応じてサブスクリプションの料金を支払います。 任意の時点での合計ユーザーがサブスクリプション数を超えない限り、サブスクリプション期間中にユーザーを追加および削除できます。
                link:
                  href: https://docs.gitlab.com/ee/subscriptions/self_managed/#subscription-seats
                  text: 座席の決定方法を学ぶ
                  ga_name: Determine how many seats you want
                  ga_location: body
              - title: セルフマネージド サブスクリプションを取得する
                text: |
                  独自の GitLab インスタンスをインストール、管理、および保守できます。 価格ページに移動し、[プレミアムを購入] または [アルティメットを購入] を選択します。
                link:
                  href: https://docs.gitlab.com/ee/subscriptions/self_managed/
                  text: 自己管理型の詳細
                  ga_name: Learn more about self-managed
                  ga_location: body
              - title: GitLab Enterprise Edition をアクティベートする
                text: |
                  ライセンスなしで新しい GitLab インスタンスをインストールすると、無料機能のみが有効になります。 GitLab Enterprise Edition (EE) でより多くの機能を有効にするには、購入時に提供されたアクティベーションコードを使用してインスタンスをアクティベートします。 アクティベーションコードは、購入確認メールまたはカスタマーポータルの[購入の管理]で確認できます。
                link:
                  href: https://docs.gitlab.com/ee/user/admin_area/license.html
                  text: アクティベーションの詳細
                  ga_name: Activate GitLab Enterprise Edition
                  ga_location: body
              - title: システム要件を確認する
                text: |
                  <p><a href="https://docs.gitlab.com/ee/install/requirements.html" data-ga-name="system rqeuirements" data-ga-location="body">サポートされているオペレーティングシステムと</a>、GitLabをインストールして使用するために必要な最小要件を確認してください。</p>
              - title: GitLabをインストールする
                text: |
                  <p><a href="https://docs.gitlab.com/ee/install/#choose-the-installation-method" data-ga-name="Installation Method" data-ga-location="body">インストール方法</a>を選択してください</p>
                  <p><a href="https://docs.gitlab.com/ee/install/#install-gitlab-on-cloud-providers" data-ga-name="your cloud provider" data-ga-location="body">クラウドプロバイダー</a> にインストールする(該当する場合)</p>
              - title: インスタンスを設定する
                text: |
                  これには、通知のためにメールを GitLab に接続する、Docker Hub からコンテナー イメージをキャッシュしてビルドを高速化できるように依存関係プロキシを設定する、認証要件を決定するなどが含まれます。
                link:
                  href: https://docs.gitlab.com/ee/install/next_steps.html
                  text: 構成できる内容を確認する
                  ga_name: Configure your self-managed instance
                  ga_location: body
              - title: オフライン環境のセットアップ (オプション)
                text: |
                  "パブリックインターネットからの分離が必要な場合にオフライン環境を設定する(通常は規制対象の業界に適用可能)"
                link:
                  href: https://docs.gitlab.com/ee/user/application_security/offline_deployments/index.html
                  text: "オフライン環境はあなたに適していますか?"
                  ga_name: Set up off-line environment
                  ga_location: body
              - title: 許可される CI/CD 共有ランナーの分数を制限することを検討してください
                text: |
                  セルフマネージド型 GitLab インスタンスのリソース使用率を制御するために、管理者は各名前空間のコンピューティング単位のクォータを設定できます。
                link:
                  href: https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#set-the-quota-of-cicd-minutes-for-a-specific-namespace
                  text: もっと詳しく
                  ga_name: Consider limiting CI/CD shared runner minutes allowed
                  ga_location: body
              - title: GitLabランナーをインストールする
                text: |
                  GitLab Runner は、GNU/Linux、macOS、FreeBSD、および Windows にインストールして使用できます。 バイナリを手動でダウンロードするか、rpm / debパッケージのリポジトリを使用して、コンテナにインストールできます。
                link:
                  href: https://docs.gitlab.com/runner/install/
                  text: インストール オプションを評価する
                  ga_name: Install GitLab runner
                  ga_location: body
              - title: GitLab ランナーを構成する (オプション)
                text: |
                  GitLabランナーは、ニーズとポリシーに合わせて構成できます。
                link:
                  href: https://docs.gitlab.com/runner/configuration/
                  text: 自己管理について学ぶ
                  ga_name: Configure GitLab runner
                  ga_location: body
              - title: 自己管理
                text: |
                  自己管理には自己管理が必要です。 管理者として、独自のニーズに合わせて調整できることはたくさんあります。
                link:
                  href: https://docs.gitlab.com/ee/administration/#configuring-gitlab
                  text: 自己管理について学ぶ
                  ga_name: Self Administration
                  ga_location: body
          - title: アプリケーションの統合 (オプション)
            copies:
              - text: |
                  シークレット管理や認証サービスなどの機能を追加したり、課題トラッカーなどの既存のアプリケーションを統合したりできます。
                link:
                  href: https://docs.gitlab.com/ee/integration/
                  text: 統合について学ぶ
                  ga_name: Integrate applications
                  ga_location: body
      - header: GitLab を使用する
        show: すべて表示
        hide: すべて隠す
        id: utiliser-gitlab
        items:
          - title: 組織を設定する
            copies:
              - text: |
                  組織とそのユーザーを構成します。 ユーザーの役割を決定し、必要なプロジェクトへのアクセス権を全員に付与します。
                link:
                  href: https://docs.gitlab.com/ee/topics/set_up_organization.html
                  text: もっと詳しく
                  ga_name: Setup your organization
                  ga_location: body
          - title: プロジェクトで作業を整理する
            copies:
              - text: |
                  GitLabでは、コードベースをホストするプロジェクトを作成できます。 また、プロジェクトを使用して、課題の追跡、作業の計画、コードでの共同作業、組み込みの CI/CD の継続的なビルド、テスト、使用によるアプリのデプロイを行うこともできます。
                link:
                  href: https://docs.gitlab.com/ee/user/project/index.html
                  text: もっと詳しく
                  ga_name: Organize work with projects
                  ga_location: body
          - title: 作業の計画と追跡
            copies:
              - text: |
                  要件、懸案事項、エピックを作成して作業を計画します。 マイルストーンを使用して作業をスケジュールし、チームの時間を追跡します。 クイックアクションで時間を節約する方法、GitLab がマークダウンテキストをレンダリングする方法、Git を使用して GitLab と対話する方法を学びます。
                link:
                  href: https://docs.gitlab.com/ee/topics/plan_and_track.html
                  text: もっと詳しく
                  ga_name: Plan and track work
                  ga_location: body
          - title: アプリケーションをビルドする
            copies:
              - text: |
                  ソースコードをリポジトリに追加し、マージリクエストを作成してコードをチェックインし、CI/CDを使用してアプリケーションを生成します。
                link:
                  href: https://docs.gitlab.com/ee/topics/build_your_application.html
                  text: もっと詳しく
                  ga_name: Build your application
                  ga_location: body
          - title: アプリケーションをセキュリティで保護する
            copies:
              - title: 使用するスキャナーを決定する
                text: |
                  GitLabは、シークレット検出、SAST、コンテナスキャンを無料利用枠で提供しています。 DAST、依存関係と IaC スキャン、API セキュリティ、ライセンス コンプライアンス、ファジングは、Ultimate 層で利用できます。 すべてのスキャナーはデフォルトでオンになっています。 個別にオフにすることもできます。
                link:
                  href: https://docs.gitlab.com/ee/user/application_security/configuration/
                  text: スキャナーの使用を構成する
                  ga_name: Determine which scanners to use
                  ga_location: body
              - title: セキュリティ ポリシーを構成する
                text: |
                  GitLab のポリシーは、指定された構成に従ってプロジェクト パイプラインが実行されるたびに、選択したスキャンの実行を要求する方法をセキュリティ チームに提供します。 したがって、セキュリティチームは、設定したスキャンが変更、変更、または無効化されていないことを確信できます。 スキャン実行とスキャン結果に対してポリシーを設定できます。
                link:
                  href: https://docs.gitlab.com/ee/user/application_security/policies/
                  text: セキュリティ ポリシーを構成する
                  ga_name: Configure your security policies
                  ga_location: body
              - title: 結合要求の承認ルールを構成する
                text: |
                  マージ要求は、マージする前に承認する必要があるように構成できます。 GitLab Free では、開発者以上の権限を持つすべてのユーザーがマージリクエストを承認できますが、これらの承認はオプションです。 GitLab Premium と GitLab Ultimate は、よりきめ細かいコントロールを設定するための柔軟性を提供します。 MRは、プロジェクトごと、およびグループレベルでのプロベールを行います。 GitLab Premium および GitLab Ultimate セルフマネージド GitLab インスタンスの管理者は、インスタンス全体の承認を構成することもできます。
                link:
                  href: https://docs.gitlab.com/ee/user/project/merge_requests/approvals/
                  text: MR 承認ルールの詳細
                  ga_name: Configure MR approval rules
                  ga_location: body
          - title: アプリケーションのデプロイとリリース
            copies:
              - text: |
                  アプリケーションを内部またはパブリックにデプロイします。 フラグを使用して、機能を段階的にリリースします。
                link:
                  href: https://docs.gitlab.com/ee/topics/release_your_application.html
                  text: もっと詳しく
                  ga_name: Deploy and release your application
                  ga_location: body
          - title: アプリケーションのパフォーマンスを監視する
            copies:
              - text: |
                  GitLabは、アプリケーションの運用と保守に役立つさまざまなツールを提供します。  チームにとって最も重要なメトリックを追跡し、パフォーマンスが低下したときに自動アラートを生成し、それらのアラートをすべてGitLab内で管理できます。
                link:
                  href: https://docs.gitlab.com/ee/operations/index.html
                  text: もっと詳しく
                  ga_name: Monitor application performance
                  ga_location: body
          - title: ランナーのパフォーマンスを監視する
            copies:
              - text: |
                  GitLabには、独自のアプリケーションパフォーマンス測定システムが付属しています。 GitLabパフォーマンスモニタリングにより、さまざまな統計を測定できます
                link:
                  href: https://docs.gitlab.com/runner/monitoring/index.html
                  text: もっと詳しく
                  ga_name: Monitor runner performance
                  ga_location: body
          - title: インフラストラクチャを管理する
            copies:
              - text: |
                  * GitLabは、インフラストラクチャ管理プラクティスを高速化および簡素化するためのさまざまな機能を提供します。
                  * GitLabは、クラウドインフラストラクチャプロビジョニングのためにTerraformと緊密に統合されているため、セットアップなしですばやく開始し、コード変更の場合と同じようにマージリクエストでインフラストラクチャの変更について共同作業を行い、モジュールレジストリを使用してスケーリングできます。
                  * GitLab と Kubernetes の統合は、クラスター アプリケーションのインストール、構成、管理、デプロイ、およびトラブルシューティングに役立ちます。
                link:
                  href: https://docs.gitlab.com/ee/user/infrastructure/index.html
                  text: もっと詳しく
                  ga_name: Manage your infrastructure
                  ga_location: body
          - title: GitLab の使用状況を分析する
            copies:
              - text: |
                  GitLabは、プロジェクト、グループ、およびインスタンスレベルで分析を提供します。 DevOps 調査および評価 (DORA) チームは、ソフトウェア開発チームのパフォーマンス指標として使用できるいくつかの主要なメトリックを開発しました。 GitLab Ultimateにはそれらが含まれています。
                link:
                  href: https://docs.gitlab.com/ee/user/analytics/index.html
                  text: もっと詳しく
                  ga_name: Analyze GitLab usage
                  ga_location: body
  next_steps:
    header: 中小企業を次のステップに導く
    cards:
      - title: カスタマーサポートが必要ですか?
        text: GitLab [documentation](https://docs.gitlab.com/){data-ga-name="documentation" data-ga-location="body"} があなたの質問に答えるかもしれません。
        avatar: /nuxt-images/icons/avatar_orange.png
        col_size: 4
        link:
          text: "カスタマーサポート"
          url: /support/
          data_ga_name: Contact support
          data_ga_location: body
      - title: さらにヘルプが必要ですか?
        text: GitLabプロフェッショナルサービスは、開始、サードパーティアプリケーションとの統合、および他のツールからの移行を支援します。
        avatar: /nuxt-images/icons/avatar_pink.png
        col_size: 4
        link:
          text: 私のPSに私に連絡してもらいましょう
          url: /sales/
          data_ga_name: Have my PS contact me
          data_ga_location: body
      - title: チャネルパートナーと協力したいですか?
        text: ディストリビューター、インテグレーター、またはマネージドサービスプロバイダー(MSP)と協力したいですか? GitLabは、[クラウドパートナー](/partners/technology-partners/){data-ga-name="cloud partners" data-ga-location="body"}のマーケットプレイスを通じて購入することもできます
        avatar: /nuxt-images/icons/avatar_blue.png
        col_size: 4
        link:
          text: チャネルパートナーディレクトリを見る
          url: https://partners.gitlab.com/English/directory/
          data_ga_name: See channel partner directory
          data_ga_location: body
      - title: アップグレードを検討していますか?
        text: |
          [Premium](/pricing/premium/){data-ga-name="why premium" data-ga-location="body"} および [Ultimate](/pricing/ultimate/){data-ga-name="why ultimate" data-ga-location="body"} の利点の詳細については、こちらをご覧ください。"
        col_size: 6
        link:
          text: なぜ究極なのか
          url: /pricing/ultimate
          data_ga_name: Why ultimate
          data_ga_location: body
      - title: サードパーティの統合を検討していますか?
        text: GitLabのオープンコアにより、統合が容易になります。 GitLabの包括的なDevSecOpsプラットフォームを補完する多くのテクノロジーパートナーがいて、独自のニーズとユースケースを満たしています。
        col_size: 6
        link:
          text: アライアンスおよびテクノロジーパートナーを見る
          url: /partners/technology-partners/
          data_ga_name: See our Alliance and Technology partners
          data_ga_location: body
