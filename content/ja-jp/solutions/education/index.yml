---
  title: 教育業界向けのGitLab
  description: GitLabは、一人ひとりが活躍できるオールインワンプラットフォームを提供し、キャンパスの近代化、イノベーション、セキュリティ強化を迅速に実現します。
  components:
    - name: 'solutions-hero'
      data:
        note:
          - 教育業界向けのGitLab
        title: ソフトウェア開発の未来はここからはじまる
        subtitle: 校内の一人ひとりにエンタープライズエッジを届けましょう
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Ultimateを無料で試す
          url: https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com/&glm_content=default-saas-trial

          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: セールスに問い合わせる
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
          icon:
            variant: product
            name: chevron-lg-right
        image:
          image_url: /nuxt-images/solutions/education/education_hero.jpeg
          image_url_mobile: /nuxt-images/solutions/education/education_hero.jpeg
          alt: ""
          bordered: true
    - name: 'education-case-study-carousel'
      data:
        case_studies:
          - logo_url: /nuxt-images/logos/uw-logo.svg
            institution_name: ワシントン大学
            quote:
              img_url: /nuxt-images/solutions/education/university_of_washington.jpg
              quote_text: この2年間で、GitLabはここワシントン大学の組織に変革をもたらしました。GitLabのプラットフォームは素晴らしいです!
              author: Aaron Timss氏
              author_title: ワシントン大学、情報学部長
            case_study_url: /customers/uw/
            data_ga_name: University of Washington case study
            data_ga_location: case study carousel
          - logo_url: /nuxt-images/case-study-logos/logo_uni_surrey.svg
            institution_name: サリー大学
            quote:
              img_url: /nuxt-images/solutions/education/university_of_surrey.jpg
              quote_text: GitLabは多くのソリューションを1か所で提供するプラットフォームであり、その点が気に入っています。GitLabは単なるリポジトリではありません。Wiki、Pages、分析、Webインターフェース、マージリクエスト、CI/CD、イシューのコメントなど、多くの機能を備えており、イシューやマージリクエスト内で共同作業を行うことができるんです。総じて素晴らしいパッケージです。
              author: Kosta Polyzos氏
              author_title: サリー大学、上級システムアドミニストレータ
            case_study_url: /customers/university-of-surrey
            data_ga_name: University of Surrey case study
            data_ga_location: case study carousel
          - logo_url: /nuxt-images/logos/victoria-university-wellington-logo.svg
            institution_name: ビクトリア大学ウェリントン(テヘレンガワカ)
            quote:
              img_url: /nuxt-images/solutions/education/victoria.jpeg
              quote_text: セルフマネージド版のGitLabについて聞いたとき、迷わずそれを選択しました。エンジニアリングのプロジェクト管理コースにおいて、自分の要件に合致するものは本当にGitLabだけでした。そしてGitLabが単一製品であったことも理由のひとつです。
              author: James Quilty博士
              author_title: ビクトリア大学ウェリントン(テヘレンガワカ)、エンジニアリング学部長
            case_study_url: /customers/victoria_university
            data_ga_name: Victoria University of Wellington case study
            data_ga_location: case study carousel
          - logo_url: /nuxt-images/logos/deakin-university-logo.svg
            institution_name: ディーキン大学
            quote:
              img_url: /nuxt-images/solutions/education/deakin-case-study.jpg
              quote_text: GitLabを導入した理由のひとつは、すぐに使える多様なセキュリティ機能を豊富に備えていることでした。これらの機能のおかげで、他のソリューションやオープンソースツール、およびそれらに関連するスキルセットを置き換えることが可能になりました。
              author: Aaron Whitehand氏
              author_title: ディーキン大学、デジタルイネーブルメント学部長
            case_study_url: /customers/deakin-university
            data_ga_name: Deakin University case study
            data_ga_location: case study carousel
          - logo_url: /nuxt-images/logos/heriot-watt-logo.svg
            institution_name: ヘリオット・ワット大学
            quote:
              img_url: /nuxt-images/solutions/education/heriot.jpeg
              quote_text: GitLabは、学生たちが職場での実践に備えるにあたり、特にアジャイル開発におけるソース管理、開発、ベストプラクティスの学習という点で重要な役割を果たしてきました。
              author: Rob Stewart氏
              author_title: ヘリオット・ワット大学、コンピュータサイエンス助教授
            case_study_url: /customers/heriot_watt_university
            data_ga_name: Heriot Watt University case study
            data_ga_location: case study carousel
          - logo_url: /nuxt-images/logos/dublin-city-university-logo.svg
            institution_name: ダブリンシティ大学
            quote:
              img_url: /nuxt-images/solutions/education/dublin-city-uni.jpg
              quote_text: "ダブリンシティ大学にとって、実習課程の一環としてGitLabを提供できることは、学生たちが進化するテクノロジーを常に把握できるいう面で大きな利点となっています。そのおかげで、業界で働くためにしっかりと準備できます。"
              author: Jacob Byrne氏
              author_title: ダブリンシティ大学、4年生
            case_study_url: /customers/dublin-city-university
            data_ga_name: Dublin City University case study
            data_ga_location: case study carousel
    - name: 'case-study-tabs'
      data:
        heading: GitLabを選ぶべき理由は?
        tabs:
          - tab: 教育機関
            title: 単科大学、総合大学、高校、プログラミング学校、または類似の機関であれ、あらゆる教育機関がソフトウェアをより迅速かつ安全に提供できるようサポートします。
            text: GitLabのDevSecOpsプラットフォームは、チームの足並みを揃え、サイクルタイムの短縮やコスト削減、セキュリティ強化、生産性アップを実現するために必要なあらゆる機能を備えています。
            cta:
              text: エキスパートに問い合わせる
              url: /sales/
              data_ga_name: talk to an expert
              data_ga_location: schools tab
            cards:
              - title: デジタルトランスフォーメーションを加速
                icon_name: slp-digital-transformation
                text: |
                  ソフトウェアデリバリのライフサイクル全体で使用可能な単一アプリケーションによって、デジタルトランスフォーメーションを成功に導きます。GitLabのDevSecOpsプラットフォームは、以下の点で役立ちます。

                  - ソフトウェアの作成および提供方法をモダン化
                  - 運用効率を向上
                  - より品質の高い製品をより迅速に提供
                  - セキュリティとコンプライアンスのリスクを低減

                cta:
                  text: 詳細を見る
                  url: /solutions/digital-transformation/
                  data_ga_name: digital transformation
                  data_ga_location: schools tab
              - title: 単一プラットフォームでコラボレーションを改善
                icon_name: slp-collaboration-alt-4
                text: |
                  分散型チーム間でのコラボレーションを目的として設計されたDevSecOpsプラットフォームを使用することで、開発者、運用チーム、セキュリティチーム、さらには教職員や学生など、すべての人が効率的に共同作業を行えるようになります。GitLabを使用すれば、次のことが可能になります。

                  - 透明性の高いイシュー、エピック、ボード、マイルストーンなどを用いたプロジェクト計画
                  - 堅牢なバージョン管理および履歴
                  - コード（およびテストとスキャンの結果）をレビューして議論するためのマージリクエスト（承認後にマージされます）
                cta:
                  text: 詳細を見る
                  url: /platform/
                  data_ga_name: platform
                  data_ga_location: schools tab
              - title: セキュリティとコンプライアンスの確保
                icon_name: slp-release
                text: |
                  GitLabのDevSecOpsプラットフォームによって、コンプライアンスを確保しながら、データ、研究、アプリケーション、学生の授業課程など、あらゆるものを保護しましょう。ビルトインのセキュリティスキャナ、SBOMの作成に役立つDependency Scanning、セキュリティおよびコンプライアンスポリシーの自動化に役立つコンプライアンスパイプラインの包括的な機能一式により、速度を犠牲にすることなく要件を満たすことができます。
                cta:
                  text: 詳細を見る
                  url: /solutions/security-compliance/
                  data_ga_name: devsecops
                  data_ga_location: schools tab
          - tab: 教職員
            title: 教室や研究室にGitLabを導入して、世界中の教職員とつながり、学生にエンタープライズの優位性を提供しましょう。
            text: GitLabは、プロジェクト管理、コラボレーション、ソース管理、git、自動化、セキュリティなどを実現するための単一プラットフォームです。使いやすさや柔軟性に優れているほか、すべてを1か所で管理できるため、キャンパス内に業界特有の実践方法を採り入れる上で最適な手段です。GitLabの教育機関向けプログラムでは、教育、学習、研究向けのGitLab無料ライセンスを要件を満たす世界各地の機関に提供しています!
            cta:
              text: 認証を受ける
              url: /solutions/education/join
              data_ga_name: get verified
              data_ga_location: teachers tab
            cards:
              - title: 教室でのGitLabの使用
                icon_name: slp-user-laptop
                text: |
                  GitLabは、コーディングだけでなく、数多くの分野で使用されています。GitLabを教室に導入することで、学生に対して、業界屈指のプラットフォームについて理解できるという利点だけでなく、プロジェクト管理、コラボレーション、バージョン管理、運用ワークフローについて学ぶ機会も提供できます。GitLabは、信頼できる唯一の情報源として、あらゆる分野におけるエンドツーエンドの学生プロジェクトに使用できます。

                  学生は、GitLabのDevSecOps機能を通じて、分野固有のスキル（ソフトウェア開発、インフラストラクチャ管理、情報技術、セキュリティ）を学ぶことができます。コードテスト、継続的インテグレーションと継続的開発、セキュリティテストをすべて1か所で実施できます。このツールを学生に提供することで、より高度な学習機会の提供を実現できます。
                cta:
                  text: 教育機関におけるGitLabの使用実態調査の結果を読む
                  url: /solutions/education/edu-survey
                  data_ga_name: gitlab education survey
                  data_ga_location: teachers tab
              - title: 研究でのGitLabの使用
                icon_name: slp-open-book
                text: |
                  GitLabは、科学界にDevSecOpsのパラダイムをもたらし、あらゆる学問分野の科学研究プロセスを変革しています。研究者は、研究プロセスにGitLabを統合することによって、透明性やコラボレーション頻度、再現性、結果に至るまでのスピード、データの整合性が向上していると報告しています。GitLab自体に関する内容、またはGitLabに保存されているデータに関して取り上げた、何千もの査読付き論文が公開されています。
                cta:
                  text: 研究事例を見る
                  url: /blog/2022/02/15/devops-and-the-scientific-process-a-perfect-pairing/
                  data_ga_name: gitlab in research
                  data_ga_location: teachers tab
              - title: キャンパスに伺います
                icon_name: slp-institution
                text: |
                  ご希望であれば、当社のチームは、DevSecOpsを導入する機関に直接出向き、教職員や学生に対してサポートを提供します。ワークショップやゲスト講演を開催して、DevSecOpsやGitLabなどをテーマに学生団体と対談することができます。
                cta:
                  text: 訪問をリクエスト
                  url: https://docs.google.com/forms/d/e/1FAIpQLSeGya0P_GOn9QXR0VrQYoy7uCSJ2x6IryrKEGBzcmQmcDZv1g/viewform
                  data_ga_name: invite us to your campus
                  data_ga_location: teachers tab
          - tab: 学生
            title: 今すぐ業界をリードするツールについて学び、雇用市場での優位性を確保しましょう。世界中の大手企業と同じように、GitLabを使用してソフトウェアをより迅速かつ安全に構築しましょう。
            text: GitLabを使用して、在学中にDevSecOpsのライフサイクル全体にわたるスキルを習得し、ポートフォリオの構築をはじめてください。まずは小さなステップからはじめて、続けていく中で理解を深めましょう。精一杯サポートいたします。
            cta:
              text: お客様の事例を読む
              url: /customers/
              data_ga_name: read case studies
              data_ga_location: students tab
            cards:
              - title: GitLabフォーラムに参加する
                icon_name: slp-chat
                text: |
                  参加して自己紹介をしましょう! プログラムメンバー向けのリソース用に、特別に「教育」カテゴリを新設しました。フォーラムは、GitLabに関するQ&Aのリソースとしてご活用ください。
                cta:
                  text: Gitlabフォーラムに移動
                  url: https://forum.gitlab.com/c/gitlab-for-education/37?_gl=1*8fqeup*_ga*MTc1MDg3NzQ5Ni4xNjU1NDAwMTk3*_ga_ENFH3X7M5Y*MTY3MTczOTQyMC4xOTQuMS4xNjcxNzQyMzI4LjAuMC4w
                  data_ga_name: gitlab forum
                  data_ga_location: students tab
              - title: ミートアップに参加または主催する
                icon_name: slp-calendar-alt-3
                text: |
                  GitLabのミートアップをチェックしましょう! お住まいの地域のプロフェッショナルに出会い、DevSecOpsについて学ぶには最適な方法です。お近くで開催されるミートアップが見つからない場合は、ミートアップを企画してみましょう。ステップはとても簡単です。
                cta:
                  text: 地元のミートアップを探す
                  url: /community/meetups
                  data_ga_name: meetups
                  data_ga_location: students tab
              - title: ハッカソンに参加する
                icon_name: slp-idea-collaboration
                text: |
                  四半期ごとに開催されるハッカソンに参加することで、GitLabコミュニティに貢献したり、賞品を獲得したり、他の貢献者とのネットワークを構築したりできます。学生のみなさまも大歓迎です!
                cta:
                  text: ハッカソンに申し込む
                  url: /community/hackathon/
                  data_ga_name: hackathon
                  data_ga_location: students tab
              - title: はじめる
                icon_name: slp-pencil
                text: |
                  GitLabの使用は初めてですか? DevSecOpsを導入するために、まずは学習リソースを確認しましょう。
                cta:
                  text: 学習リソース
                  url: https://docs.gitlab.com/ee/tutorials/
                  data_ga_name: getting started
                  data_ga_location: students tab
              - title: 貢献者になる
                icon_name: slp-code
                text: |
                  現在学生であり、経験を積みたいとお考えの場合や、サポートネットワーク内でのスキル習得を目指す場合は、まずはぜひGitLabのコード貢献者プログラムをご確認ください!
                cta:
                  text: 貢献者になる
                  url: /community/contribute
                  data_ga_name: contribute
                  data_ga_location: students tab
    - name: 'education-feature-tiers'
      data:
        heading: キャンパスにGitLabを導入する
        teacher_tier:
          title: 教職員
          text: 教育、学習、研究を支援するための無料の教育向けライセンスを取得しましょう*
          features:
            - item: 無制限のライセンス
            - item: Ultimateプラン
            - item: あらゆるデプロイ手法に対応
            - item: 'コンピュートユニット: 50,000'
            - item: コミュニティサポート
            - item: 教室または研究での使用限定
          cta:
            text: 認証を受ける
            url: /solutions/education/join/
            data_ga_name: get verified
            data_ga_location: education feature tiers
          disclaimer: '*機関の経営、管理、運営のための使用は許可されません。'
        schools_tier:
            title: 教育機関
            text: 単一価格のシンプルな料金体系で、キャンパス全体で完全なDevSecOpsプラットフォームを利用できます
            features:
              - item: 無制限のライセンス**
              - item: Ultimateプラン
              - item: あらゆるデプロイ手法に対応
              - item: 'コンピュートユニット: 50,000'
              - item: 優先サポート
              - item: ユースケースの制限なし
            cta:
              text: セールスに問い合わせる
              url: /sales/
              data_ga_name: contact sales
              data_ga_location: education feature tiers
            disclaimer: '**在校生の登録のみ。'
            callout_box:
              title: 特定のニーズに合わせてプランをカスタマイズすることもできます
              features:
                - item: 必要なライセンス数だけ追加
                - item: 任意のプラン
                - item: あらゆるデプロイ手法に対応
                - item: 購入プランに応じたコンピュートユニット
                - item: 優先サポート
                - item: ユースケースの制限なし
              cta:
                text: 20%割引を適用
                url: /sales/
                data_ga_name: get discount
                data_ga_location: education feature tiers
    - name: 'education-stats'
      data:
        header: GitLabコミュニティに参加する
        stats:
          - spotlight_text: 1,000以上
            text: GitLabの教育機関向けプログラムに参加中の教育機関の数
          - spotlight_text: 300万人
            text: 教育機関におけるGitLabユーザーの数（増加中）
          - spotlight_text: '65か国'
            text: GitLabの教育機関向けプログラムが利用されている国の数