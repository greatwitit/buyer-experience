---
  title: GitLabとJira
  description: GitLabからJiraまで仕事を自動化する
  components:
    - name: 'solutions-hero'
      data:
        title: GitLabとJira
        subtitle: GitLabからJiraまで仕事を自動化する
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          url: /learn/#agile_management
          text: 学習を開始
          data_ga_name: start learning
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "イメージ: GitLabとJira"
    - name: featured-media
      data:
        column_size: 4
        media:
          - title: GitLab<->Jiraインテグレーション
            aos_animation: fade-up
            aos_duration: 500
            text: |
              GitLabプロジェクトをJiraインスタンスと統合すると、GitLabプロジェクトとJira内の任意のプロジェクトとの間のアクティビティを自動的に検出し、クロスレファレンスすることが可能になります。
            icon:
              name: kanban
              alt: かんばん
              variant: marketing
          - title: Jira開発パネルのインテグレーション
            aos_animation: fade-up
            aos_duration: 1000
            text: |
              既存のJiraプロジェクト統合を補完できるよう、GitLabプロジェクトをJira開発パネルと統合できるようになりました。
            icon:
              name: computer-test
              alt: Jira開発パネルのインテグレーション
              variant: marketing
          - title: JiraからGitLabへの移行
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              GitLabは、SCM、CI/CD、セキュリティなどをホストする1つのプラットフォームにアジャイル計画をもたらす堅牢なプロジェクト管理ツールを提供します。
            icon:
              name: scale
              alt: Jira開発パネルのインテグレーション
              variant: marketing
    - name: featured-media
      data:
        header: GitLab-Jiraインテグレーションの仕組み
        column_size: 4
        media:
          - title: GitLab<->Jira基本インテグレーション
            aos_animation: zoom-in-up
            aos_duration: 500
            text: |
              * コミットメッセージまたはMR (マージリクエスト)でJiraイシューIDをメンションします。
              
              * コミットまたはMRが特定のJiraイシューを解決または完了することをメンションします。
              
              * GitLabでJiraイシューを直接表示します。
            image:
              url: /nuxt-images/enterprise/source-code.jpg
              alt: "GitLab <-> Jira"
          - title: Jira開発パネルのインテグレーション
            aos_animation: zoom-in-up
            aos_duration: 1000
            text: |
              * 関連するGitLabマージリクエスト、ブランチ、およびコミットにJiraイシューから直接簡単にアクセスします。
              
              * Jira CloudがホストするJiraと統合された自己管理GitLabまたはGitLab.comで動作します。
              
              * トップレベルグループまたは個人の名前空間内のすべてのGitLabプロジェクトをJiraインスタンスのプロジェクトに接続します。
            image:
              url: /nuxt-images/enterprise/ci-cd.jpg
              alt: "パネルインテグレーション"
          - title: JiraからGitLabへの移行
            aos_animation: zoom-in-up
            aos_duration: 1500
            text: |
              * JiraイシューをGitLab.comまたは自己管理のGitLabインスタンスにインポートします。
              
              * タイトル、説明、ラベルを直接インポートします。
              
              * JiraユーザーをGitLabプロジェクトメンバーにマップします。
            image:
              url: /nuxt-images/enterprise/agile2.jpg
              alt: "GitLabからJiraへの移行"
    - name: featured-media
      data:
        header: ハウツー動画
        text: |
          GitLab-Jiraインテグレーションに関するリソースの中から、実装を理解する際に特に役立ちそうなものをまとめました。
        column_size: 4
        media:
          - title: GitLab-Jira基本インテグレーション
            aos_animation: fade-up
            aos_duration: 500
            text: |
              JiraからGitLabイシューへのコンテンツとプロセスの移行はいつでも可能ですが、Jiraを引き続きGitLabと一緒に使用することもできます。
            link:
              text: 読み取り
              href: https://docs.gitlab.com/ee/integration/jira/
              data_ga_name: Gitlab-Jira integration
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/fWvwkx5_00E?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: GitLab-Jira開発パネル
            aos_animation: fade-up
            aos_duration: 1000
            text: |
              既存のJiraプロジェクト統合を補完できるよう、GitLabプロジェクトをJira開発パネルと統合できるようになりました。
            link:
              text: 読み取り
              href: https://docs.gitlab.com/ee/integration/jira/
              data_ga_name: Gitlab-Jira development panel
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/VjVTOmMl85M?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: Jiraプロジェクトの問題をGitLabにインポートする
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              GitLab Jiraインポート機能を使用すると、JiraイシューをGitLab.comまたは自己管理のGitLabインスタンスにインポートできます。
            link:
              text: 読み取り
              href: https://docs.gitlab.com/ee/user/project/import/jira/
              data_ga_name: Import jira issue to GitLab
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/cixs4NJB3H8?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: JiraイシューリストをGitLabで表示する
            aos_animation: fade-up
            aos_duration: 500
            text: |
              Jiraを主な作業追跡ツールとして使用している組織では、貢献者が複数のシステムで作業する場合、単一の信頼できる情報源を維持することは困難な場合があります。
            link:
              text: 読み取り
              href: https://docs.gitlab.com/ee/user/project/import/jira/
              data_ga_name: View jira issue list in GitLab
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/_yDcD_jzSjs?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: イシューをインポートするときに、JiraユーザーをGitLabユーザーにマップします。
            aos_animation: fade-up
            aos_duration: 1000
            text: |
              JiraからGitLabにイシューをインポートするとき、インポートを実行する前にJiraユーザーをGitLabプロジェクトメンバーにマップできるようになりました。これにより、インポート担当者は、GitLabに移動するイシューに対して正しいレポーターと担当者を設定できます。
            link:
              text: 読み取り
              href: https://docs.gitlab.com/ee/user/project/import/jira/
              data_ga_name: Map jira users to GitLab
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/cixs4NJB3H8?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: GitLabプロジェクト管理ロードマップ
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              GitLabとJiraとの統合の改善に常に取り組んでいます。皆さまからのフィードバックを歓迎しています。次のリリースで何が予定されているかを是非ご確認ください。
            link:
              text: 読み取り
              href: https://gitlab.com/groups/gitlab-org/-/epics/2738/
              data_ga_name: GitLab project management roadmap
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/bT60rJEoWhw?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
    - name: logo-links
      data:
        header: オープンソースパートナー
        column_size: 2
        logos:
          - logo_url: /nuxt-images/enterprise/logo-dish.svg
            logo_alt: ディッシュ
            aos_animation: zoom-in
            aos_duration: 200
          - logo_url: /nuxt-images/enterprise/logo-expedia.svg
            logo_alt: エクスペディア
            aos_animation: zoom-in
            aos_duration: 400
          - logo_url: /nuxt-images/enterprise/logo-goldman-sachs.svg
            logo_alt: Goldman Sachs
            aos_animation: zoom-in
            aos_duration: 600
          - logo_url: /nuxt-images/enterprise/logo-nasdaq.svg
            logo_alt: Nasdaq
            aos_animation: zoom-in
            aos_duration: 800
          - logo_url: /nuxt-images/enterprise/logo-uber.svg
            logo_alt: Uber
            aos_animation: zoom-in
            aos_duration: 1000
          - logo_url: /nuxt-images/enterprise/logo-verizon.svg
            logo_alt: Verizon
            aos_animation: zoom-in
            aos_duration: 1200
