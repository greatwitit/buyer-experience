---
  title:  Software. Faster.
  description: GitLabによりDevSecOpsが簡素化され、重要な作業にのみ集中できるようになります。 詳細はこちらをご覧ください。
  image_title: /nuxt-images/open-graph/open-graph-gitlab.png
  twitter_image: /nuxt-images/open-graph/open-graph-gitlab.png
  hero:
    title: |
      アイデア段階からリリースまで 瞬時のスピードでソフトウェアを構築
    aos_animation: fade-down
    aos_duration: 1600
    aos_offset: 0
    image:
      url: /nuxt-images/software-faster/hero.png
      alt: ソフトウェア高速ヒーローのイメージ
      aos_animation: fade-up
      aos_duration: 1600
      aos_offset: 0
    secondary_button:
      video_url: https://player.vimeo.com/video/799236905?h=59b06b0e99&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
      text: GitLabとは？（動画を見る）
      data_ga_name: watch video
      data_ga_location: hero
  customer_logos:
    showcased_enterprises:
      - image_url: "/nuxt-images/home/logo_tmobile_mono.svg"
        link_label: Link to T-Mobile and GitLab webcast landing page"
        alt: "T-Mobileロゴ"
        url: https://learn.gitlab.com/c/learn-how-t-mobile-i?x=04KSqy
      - image_url: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
        link_label: Goldman Sachsお客様事例へのリンク
        alt: "Goldman Sachsロゴ"
        url: "/customers/goldman-sachs/"
      - image_url: "/nuxt-images/home/logo_cncf_mono.svg"
        link_label: Link to Cloud Native Computing Foundation customer case study
        alt: "クラウドネガティブロゴ"
        url: /customers/cncf/
      - image_url: "/nuxt-images/home/logo_siemens_mono.svg"
        link_label: Siemensお客様事例へのリンク
        alt: "Siemensロゴ"
        url: /customers/siemens/
      - image_url: "/nuxt-images/home/logo_nvidia_mono.svg"
        link_label: Nvidiaお客様事例へのリンク
        alt: "Nvidiaロゴ"
        url: /customers/nvidia/
      - alt: UBSロゴ
        image_url: "/nuxt-images/home/logo_ubs_mono.svg"
        url: https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/
        link_label: UBSお客様事例へのリンク
  featured_content:
    col_size: 4
    header: "GitLabを活用しソフトウェアデリバリーの迅速化を実現"
    case_studies:
      - header: Nasdaq社の クラウドへの迅速かつシームレスな移行
        description: |
          Nasdaq社は、100%クラウドへの移行を目指しており、それを実現するためにGitLabと提携しています。
        showcase_img:
          url: /nuxt-images/software-faster/nasdaq-showcase.png
          alt: Nasdaq logo on a window
        logo_img:
          url: /nuxt-images/enterprise/logo-nasdaq.svg
          alt: Nasdaq logo
        link:
          video_url: https://player.vimeo.com/video/767082285?h=e76c380db4
          text: この事例に関する動画を見る
          data_ga_name: nasdaq
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 800
      - header: デプロイ時間を5倍速く
        description: |
          Hackerone社は GitLab Ultimateを使用してパイプラインの改善、デプロイの高速化、開発者の作業効率の改善を実現しました。
        showcase_img:
          url: /nuxt-images/software-faster/hackerone-showcase.png
          alt: コンピュータでコードの作業をしている人 - HackerOne
        logo_img:
          url: /nuxt-images/logos/hackerone-logo.png
          alt: HackerOneロゴ
        link:
          href: /customers/hackerone/
          text: 事例を読む
          data_ga_name: hackerone
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 1000
      - header: 144倍速く機能をリリース
        description: |
          Airbus Intelligence社は、単一のアプリケーションCIでワークフローとコード品質を改善しました。
        showcase_img:
          url: /nuxt-images/software-faster/airbus-showcase.png
          alt: 飛行機の翼 - Airbus
        logo_img:
          url: /nuxt-images/software-faster/airbus-logo.png
          alt: Airbusロゴ
        link:
          href: /customers/airbus/
          text: 事例を読む
          data_ga_name: airbus
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 1200
  devsecops:
    header: 1つの包括的なプラットフォームで 重要なDevSecOpsツールをすべてカバー
    link:
      text: 詳細はこちら
      data_ga_name: platform
      data_ga_location: body
    cards:
      - header: 洞察の向上
        description: ソフトウェアデリバリーライフサイクル全体のエンドツーエンドの可視性。
        icon: case-study-alt
      - header: 効率アップ
        description: 作業の自動化のみならず、他のサービスやツールともシームレスな連携をサポート。
        icon: principles
      - header: 共同作業の改善
        description: 開発者、セキュリティチーム、運用チームを統合する1つのワークフロー。
        icon: roles
      - header: 価値を生み出すまでの時間を短縮
        description: 持続的な改善を促進する高速なフィードバック。
        icon: verification
  by_industry_case_studies:
    title: DevSecOpsに関するその他のコンテンツ
    charcoal_bg: true
    header_animation: fade-up
    header_animation_duration: 500
    row_animation: fade-right
    row_animation_duration: 800
    rows:
      - title: 2023年グローバルDevSecOpsレポートシリーズ
        subtitle: 5,000人以上のDevSecOpsプロフェッショナルから学んだ、ソフトウェア開発、セキュリティ、運用の現状についての洞察をご覧ください。
        image:
          url: /nuxt-images/software-faster/devsecops-survey.png
          alt: devsecopsアンケート
        button:
          href: /developer-survey/
          text: レポートを読む
          data_ga_name: devsecops survey
          data_ga_location: body
        icon:
          name: doc-pencil-alt
          alt: プレイサークルのアイコン
      - title: 'ケイデンス（リズムやタイミング）がすべて: エンジニアが10倍の成果を上げられる、10倍のエンジニアリング組織'
        subtitle: GitLabのCEOで共同創業者であるSid Sijbrandijが、エンジニアリング組織において、適切なタイミングで進行し、調和することの重要性について語りました。
        image:
          url: /nuxt-images/blogimages/athlinks_running.jpg
          alt: マラソンを走っている人たちの写真
        button:
          href: /blog/2022/11/03/cadence-is-everything-10x-engineering-organizations-for-10x-engineers/
          text: ブログを読む
          data_ga_name: read the blog
          data_ga_location: body
        icon:
          name: blog
          alt: プレイサークルのアイコン
