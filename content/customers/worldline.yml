---
  data:
    title: Worldline
    description: 'The power of collaboration decreases cycle time and improves release quality'
    og_title: Worldline
    twitter_description: 'The power of collaboration decreases cycle time and improves release quality'
    og_description: 'The power of collaboration decreases cycle time and improves release quality'
    og_image: /nuxt-images/blogimages/worldline-case-study-image.jpg
    twitter_image: /nuxt-images/blogimages/worldline-case-study-image.jpg
    customer: Worldline
    customer_logo: /nuxt-images/logos/worldline_logo_new.png
    heading: 'Worldline improves code reviews’ potential by 120x'
    key_benefits:
      - label: Easy code reviews
        icon: magnifying-glass-code
      - label: Increase in code quality
        icon: increase-thin
      - label: Easier review/planning
        icon: agile
    header_image: /nuxt-images/blogimages/worldline-case-study-image.jpg
    customer_industry: Financial Services
    customer_employee_count: 11,000 worldwide
    customer_location: Offices in Europe, Asia, and Latin America
    customer_solution: |
      [GitLab Free](/pricing/){data-ga-name="free solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: GitLab projects
        stat: 14,500
      - label: merge requests
        stat: 100,000+
      - label: CI jobs executed in 1 year
        stat: 1.4M
    blurb: Payment service provider Worldline was looking for a way to increase collaboration and reduce review cycle time in its development phase.
    introduction: |
        With GitLab, Worldline has decreased cycle time and improved release quality.
    quotes:
      - text: |
          GitLab is the backbone of our development environment. Today we have 2,500 people working on that for us daily.
        author: Antoine Neveux
        author_role: Software Engineering- Kazan Team
        author_company: Worldline
      - text: |
          Because we’ve got lots of requests from our users ... most of our users are developers and they are enthusiastic about everything that is happening in the open source world. We’ve got lots and lots of people who are really aware of what GitLab is providing. They are using GitLab.com and they are all following the progress of what GitLab is doing.
        author: Antoine Neveux
        author_role: Software Engineering- Kazan Team
        author_company: Worldline
    content:
      - title: Powering billions of financial transactions annually
        description: |
          Worldline is the European market leader in the payment and transaction services industry. With innovation at the core of its DNA, Worldline core offerings include pan-European and domestic Commercial Acquiring for physical or online businesses, secured payment transaction processing for banks and financial institutions, as well as transactional services in e-Ticketing and for local and central public agencies.
          
          Worldline processes billions of financial transactions each year. Covering the entire payment value chain, the company’s technological experts create and operate digital platforms that handle the millions of highly critical transactions between a company, its partners, and its customers. The company focuses on three pillars of business: Merchant Services, Mobility & e-Transactional Services, and Financial Services.
      - title: Increasing speed and collaboration with the capabilities of Git
        description: |
          In 2014, the company was looking for a way to increase collaboration and reduce review cycle time in its development phase. One way was to use a chain consisting of many tools including Gerrit, ReviewBoard, CVS, Jenkins, and Subversion. For its branch management capabilities, Git was also used to improve the manual release process.

          “Our model required having to request the manual creation of an SVN repository which could take up to a week. And then we had to use that to work together and we were not doing any code reviews,” said Antoine Neveux, Software Engineering - Kazan Team, Worldline. “And some projects, when they were out of the scope for typical Jenkins jobs, were unable to use Continuous Integration.”
      - title: An easy-to-use solution encourages code reviews
        description: |
          To help overcome these challenges, Git capabilities were introduced into the development environment. Worldline started with a vanilla Git product, but it was quickly clear that it wasn’t going to meet the required needs.

          “We started using GitLab because we wanted to get an easy Git repository management system and because we wanted people to be able to use merge requests,” Neveux explained. “We wanted the ability to have more code reviews and to ease discussions between developers.”

          The adoption of GitLab was quite successful and, within six months, over 1,000 users were active users. Developers explained that the adoption rate was high because GitLab is so easy to use. People actually felt encouraged to contribute code reviews with GitLab merge requests. Previous code review tools had 10-20 developers using them, while Worldline currently has 3,000 active users of GitLab — an adoption rate increase of 12,000 percent.
      - title: New projects, new opportunities
        description: |
          Worldline now hosts 14,500 projects on GitLab. It previously took 1-2 weeks to get a source code repo; now it takes a few seconds. The company is using GitLab’s CI and merge requests as well as GitLab Pages and Mattermost capabilities. They are also exploring deployments and integration with Kubernetes. Shared runners have helped increase developer acceptance. Before GitLab, Worldline had 15,000 Jenkins jobs running. When GitLab introduced CI, Worldline moved over because GitLab allows users to run continuous integration in separate containers. Now Worldline is running close to 80% of their CI through GitLab.

          “Thanks to GitLab CI we allow lots of new projects to come like C++ and .net projects or mobile projects thanks to the fact that people can bring their own runners. That is one of the biggest changes,” Neveux said.

          GitLab Pages has also improved the way Worldline communicates. Using a static website generator, creating a website from scratch is easy. “You can’t even believe how much of our website is created with Pages — and our documentation too actually,” Neveux explained. “People moved like all of the wikis and other things like this to GitLab Pages and so it is the standard for everyone to communicate. Everything is published through pages and it is starting to become collaborative, so that is one major change as well.”

          Worldline began using GitLab in 2014, and both companies have experienced incredible growth and change during this time. With the addition of Git at Worldline, users started looking at each other's code and then they started collaborating on each other’s projects. This was a big step for the organization and has changed how teams focus and work. Now the company is utilizing innersourcing practices and has people creating frameworks, tools, and best practices documents that they share within the company.
