---
  data:
    customer: Iron Mountain
    customer_logo: /nuxt-images/home/logo_iron_mountain_mono.svg
    heading: Iron Mountain drives DevOps evolution with GitLab Ultimate
    key_benefits:
      - label: Reduced complexity
        icon: bulb-bolt
      - label: Streamlined security
        icon: secure-alt-2
      - label: Improved visibility
        icon: eye-magnifying-glass
    header_image: /nuxt-images/blogimages/iron-mountain-2.png
    customer_industry: Technology
    customer_employee_count: 25,000+
    customer_location: Boston, Massachusetts
    customer_solution: |
      [GitLab Ultimate](/pricing/ultimate/){data-ga-name="ultimate solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: in approximate cost savings per year
        stat: $150k
      - label: saved in onboarding time per project
        stat: 20 hrs
    blurb: Iron Mountain has embraced Agile methods — and that has led the data governance giant to GitLab CI/CD.
    introduction: |
      The platform speeds pipeline creation and takes over administrative chores so that developers can focus on development.
    quotes:
      - text: |
          GitLab has provided us with the foundation and platform to enable our scaled Agile framework. We are able to collaborate within our Enterprise IT teams and our key stakeholders.
        author: Hayelom Tadesse
        author_role: Vice President of Enterprise Technology
        author_company: Iron Mountain
    content:
      - title: ''
        description: |
          Iron Mountain Incorporated is a global leader in innovative storage, data center infrastructure, asset lifecycle management and information management services. Founded in 1951 and trusted by more than 225,000 customers worldwide, Iron Mountain helps customers CLIMB HIGHER™ to transform their businesses. 
          
          Through a range of services including digital transformation, data centers, secure records storage, information management, asset lifecycle management, secure destruction, and art storage and logistics, Iron Mountain helps businesses bring light to their dark data, enabling customers to unlock value and intelligence from their stored digital and physical assets at speed and with security, while helping them meet their environmental goals.
      - title: Taming fragmented tooling to gain a single view of DevOps
        description: |
          Iron Mountain’s architecture and platform group had a goal to implement an overall strategy to support the use of cloud managed services. The group began evaluating SaaS-based integration platforms, which they hoped could address challenges such as fragmented open source tooling, gaps that blocked development and operations communications, heavy administrative burdens, and difficulties with efficiently securing complex pipeline deployments. Existing on-premises Jira software incurred management complexity in the form of plugin troubleshooting, and with Veracode security software, teams were only able to discover coding issues late in the development cycle, causing resource-intensive rework. In addition, operations needed to support teams’ increasing adoption of new Kubernetes development methods. Iron Mountain’s technical leadership concluded that an appropriate cloud services solution was needed to support developers’ challenges.
      - title: GitLab Ultimate SaaS empowers developers while reducing complexity
        description: |
          Iron Mountain chose [GitLab Ultimate on Google Cloud](/partners/technology-partners/google-cloud-platform/){data-ga-name="google cloud" data-ga-location="customers content"} to solve these challenges, and is now running 240 automated cloud deployments on GCP for Google Kubernetes Engine (GKE) workloads. GitLab CI/CD allows teams to save time and boost efficiency with automation while also supporting better security assurance via up-front security scanning such as dynamic application security testing (DAST) and static application security testing (SAST). 
          
          GitLab supports Iron Mountain’s goal to “shift security left” — placing more security responsibility in the hands of developers and empowering teams to find security flaws earlier in the process. 
          
          GitLab also serves as a complete, cohesive orchestration environment that simplifies management while meeting developers’ needs as they build new Kubernetes environments to support cloud and multi-cloud architecture. This single solution reduces complexity and supports a future-proof, community-driven roadmap that aligns with Iron Mountain’s tech planning. Iron Mountain sees GitLab as an important part of Enterprise Architecture and Platforms’ enablement of Agile methods and helpful to the company’s evolution to DevOps. GitLab Ultimate SaaS does the maintenance, so developers can focus on development.
      - title: Building a governance model with GitLab
        description: |
          With GitLab Ultimate SaaS, Iron Mountain has been able to reduce the costs associated with infrastructure management while also securely increasing production velocity. The company has cut the number of on-premises virtual machines (VMs) by nearly half, saving more than $60,000 per year in maintenance costs and more than $90,000 per year in labor. 

          “GitLab has provided us with the foundation and platform to enable our scaled Agile framework,” adds Hayelom Tadesse, Iron Mountain’s vice president of enterprise technology. “We are able to collaborate within our Enterprise IT teams and our key stakeholders.”

          Epics in GitLab provide a way for Iron Mountain teams to organize and track issues according to a strategic theme. Team members can employ features within epics to create a governance model for development that supports large, multiyear initiatives.

          Iron Mountain Vice President of Technology Jason Monoharan says GitLab — the company — has a roadmap and community spirit that aligns with best Agile practices. “To me, the vision that GitLab has in terms of tying strategy to scope and to code is very powerful. Also, I appreciate the level of investment they are continuing to make in the platform as they continue to work on it,” he says. Monoharan adds that GitLab’s team has collaborated closely with Iron Mountain teams to define priority features. 

          At Iron Mountain, GitLab’s single pane of glass helps teams to understand the demands on their capacity, and allows them to focus on priority work in order to speed secure delivery of innovative services — all while ensuring the security of pipelines and improving productivity.
