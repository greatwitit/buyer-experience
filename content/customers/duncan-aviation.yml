data:
  customer: Duncan Aviation
  customer_logo: /nuxt-images/logos/duncan-aviation-logo.svg
  heading: >
    GitLab ensures confident automation and compliance, secures deployments at
    Duncan Aviation
  header_image: /nuxt-images/blogimages/duncan_aviation_cover_image.jpg
  customer_industry: Aviation
  customer_location: Lincoln, NE
  customer_employee_count: |
    2,761
  customer_solution: |
    [GitLab SaaS Ultimate](/pricing/ultimate/){data-ga-name="ultimate solution" data-ga-location="customers hero"}
  key_benefits:
      - label: Conserved admin resources
        icon: time-is-money
      - label: Ensured vulnerability checks
        icon: shield-check-light
      - label: Automated manual processes
        icon: accelerate
  sticky_benefits:
    - stat: 20x
      label: faster deploy time in Development environment
    - stat: 9x
      label: faster deploy time in Production environment
    - stat: 1,339
      label: hours saved annually across teams
  blurb: >
    Duncan Aviation relies on GitLab Ultimate's SaaS offering to streamline
    software deployments and adapt to evolving security and compliance
    requirements
  introduction: >
    This leading aviation services company relies on GitLab Ultimate's SaaS
    offering to streamline software deployments and adapt to evolving security
    and compliance requirements. 
  quotes:
    - text: GitLab helped us to automate manual processes using pipelines. Now we are
        deploying code regularly, getting essential changes and fixes to our
        customers a lot faster
      author: Ben Ferguson
      author_role: Senior Programmer, Duncan Aviation
      author_company: Duncan Aviation
  content:
    - title: Duncan Aviation deploys software that supports its jet services
        offerings, including maintenance, repair and operations
      description: >-
        
        [Duncan Aviation](https://www.duncanaviation.aero/) is the world's largest privately owned jet service provider, maintaining jets for business and government agencies, as well as for other aircraft service providers. Services include 24x7 system troubleshooting, repairs, and overhauls. The company's systems allow clients to track the progress of work being done on their aircraft. The company, founded in 1956, has three full-service facilities located in Battle Creek, MI, Lincoln, NE, and Provo, UT.   Duncan Aviation works to comply with Cybersecurity Maturity Model Certification (CMMC) guidelines, in keeping with federal guidelines, to better secure software infrastructure. To do this, they establish and enforce security configuration settings for information technology products employed in the organization's systems. Secure configurations are an important aspect of Duncan Aviation's competitive advantage. 
    - title: Automate and speed builds, while meeting evolving security compliance
        goals
      description: >-
        

        As development teams looked to accelerate software delivery, they quickly concluded that their collection of in-house deployment tools were not effectively scaling. Deploying code to a production environment required manual builds, testing, and deploying, which were prone to human error. Due to the lack of concrete documentation of what was actually deployed, errors were often solved by a redeploy rather than digging into the problem. Development managers knew automated scanning, tests, and linting were required. At the same time, Duncan Aviation's efforts to implement emerging government guidelines for secure applications highlighted its lack of dependable, repeatable, and full-featured deployment processes. 
        

        To solve these problems, the company needed a single-solution, end-to-end DevOps platform. A tech team at Duncan Aviation appraised a host of DevOps options, including both GitLab and GitHub. The team chose GitLab Ultimate. The SaaS-based platform, which is ideal for organizations looking to optimize and accelerate delivery while managing priorities, security, risk, and compliance, was brought on, in part, to scan for and identify vulnerabilities, and perform auditing and documentation. Choosing GitLab helped Duncan Aviation ensure compliance, while monitoring deployment status and providing insights into, and feedback on, pipeline issues.
    - title: Agile methods take off with help from GitLab
      description: >-
        

        GitLab Ultimate enables Duncan Aviation teams to conserve development and administration resources and implement Agile DevOps methods, while advancing shift-left security, enabling them to identify and fix defects much earlier in the software development lifecycle. The platform also automates formerly manual pipeline and scanning processes. “GitLab does everything we need it to. It ensures compliance, automates testing, and implements our changes quickly and consistently,” said Ben Ferguson, senior programmer/analyst at Duncan Aviation. “As a result, we worry less about implementing code and more about solving problems for our customers.” 
        

        Using GitLab's DevOps platform, Duncan Aviation's production time dropped on average from 45 minutes to 5 minutes. Regular platform updates are assured, giving Duncan Aviation immediate access to all of the platform's latest security, automation, collaboration, and online learning features. The platform also provides visibility across projects, reducing development time from 20 minutes to just 1 minute. GitLab also decreases administrative burdens, annually saving the team approximately 1,339 hours.  Many CMMC requirements are addressed with GitLab, including efficient and fast identification and correction of system flaws; enforcement of security configuration settings for tools used in organizational systems; tracking of log changes; and analysis of the security impact caused by implementation changes.
    - title: Code insight drives software confidence and secures vital aviation
        services
      description: >-
        
        With GitLab Ultimate, Duncan Aviation saves time on system administration requirements. Its continuous integration and source code management capabilities streamline processes, supporting Agile methods. Team members can see pipeline issues, as well as security reports, all in one place, without the need to load and access multiple tools. Complex software fixes that could have delayed status updates for clients now are seamless and immediate. 
        
        
        GitLab security scanning has made Duncan Aviation teams more aware of vulnerability issues at the development stage as well as throughout the software lifecycle, positively impacting practices, according to Ferguson. “We deploy a lot more code than we used to, and it's a lot easier to deploy,” he said. “GitLab has helped us with our confidence that everything is working correctly. GitLab helps us, because we have more insight as to what code is running and where.” All and all, GitLab empowers teams, mitigates risk, differentiates customer services, and keeps Duncan Aviation software efforts at the forefront of change.

