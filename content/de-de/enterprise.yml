---
  title: GitLab für Unternehmen – Zusammenarbeit leicht gemacht
  description: Beschleunigen Sie die Bereitstellung von Unternehmenssoftware mit der GitLab DevSecOps-Plattform, senken Sie Ihre Entwicklungskosten und optimieren Sie die Zusammenarbeit im Team.
  image_title: /nuxt-images/open-graph/open-graph-enterprise.png
  side_navigation_links:
    - title: Übersicht
      href: '#overview'
    - title: Vorteile
      href: '#benefits'
    - title: Leistungen
      href: '#capabilities'
    - title: Fallstudien
      href: '#case-studies'
  solutions_hero:
    title: GitLab für Unternehmen
    subtitle: Die umfangreichste DevSecOps-Plattform, von der Planung bis zur Produktion. Arbeiten Sie unternehmensweit zusammen, stellen Sie sicheren Code schneller bereit und erzielen Sie bessere Geschäftsergebnisse.
    header_animation: fade-down
    header_animation_duration: 800
    buttons_animation: fade-down
    buttons_animation_duration: 1200
    img_animation: zoom-out-left
    img_animation_duration: 1600
    primary_btn:
      text: Kostenlose Testversion starten
      url: https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com
      data_ga_name: free trial
      data_ga_location: header
    secondary_btn:
      text: Sprechen Sie mit einem Experten
      url: /sales
      data_ga_name: talk to an expert
      data_ga_location: header
    image:
      image_url: /nuxt-images/enterprise/enterprise-header.jpeg
      alt: "Transporter wird mit Kisten beladen"
      rounded: true
  by_industry_intro:
    logos:
      - name: Siemens
        image: /nuxt-images/logos/logo_siemens_color.svg
        aos_animation: zoom-in-up
        aos_duration: 200
        url: /customers/siemens/
        aria_label: Link zur Siemens Kunden-Fallstudie
      - name: hilti
        image: /nuxt-images/customers/hilti-logo.png
        aos_animation: zoom-in-up
        aos_duration: 400
        url: /customers/hilti/
        aria_label: Link zur Hilti Kunden-Fallstudie
      - name: Bendigo
        image: /nuxt-images/customers/babl_logo.png
        aos_animation: zoom-in-up
        aos_duration: 600
        url: /customers/bab/
        aria_label: Link zur Bendigo Kunden-Fallstudie
      - name: Radio France
        image: /nuxt-images/logos/logoradiofrance.svg
        aos_animation: zoom-in-up
        aos_duration: 800
        url: /customers/radiofrance/
        aria_label: Link zur Radio France Kunden-Fallstudie
      - name: Credit agricole
        image: /nuxt-images/customers/credit-agricole-logo-1.png
        aos_animation: zoom-in-up
        aos_duration: 1000
        url: /customers/credit-agricole/
        aria_label: Link zur Credit Agricole Kunden-Fallstudie
      - name: Kiwi
        image: /nuxt-images/customers/kiwi.png
        aos_animation: zoom-in-up
        aos_duration: 1200
        url: /customers/kiwi/
        aria_label: Link zur Kiwi Kunden-Fallstudie
  by_solution_intro:
    aos_animation: fade-up
    aos_duration: 800
    text:
      highlight: Unternehmen verlassen sich auf DevSecOps, damit ihre Teams Software schneller bereitstellen können.
      description: Was für einzelne Projekte gut funktioniert hat, lässt sich nur schwer unternehmensweit skalieren. Im Gegensatz zu herkömmlichen Toolchains, die auf Einzellösungen basieren, ermöglicht GitLab Teams eine schnellere Iteration und Zusammenarbeit, beseitigt die Komplexität und das Risiko und bietet alles für eine schnellere Bereitstellung hochwertigerer und sicherer Software.
  by_solution_benefits:
    title: DevSecOps im großen Maßstab
    is_accordion: true
    right_block_animation: zoom-in-left
    right_block_duration: 800
    left_block_animation: zoom-in-right
    left_block_duration: 800
    header_animation: fade-up
    header_duration: 800
    image:
      image_url: /nuxt-images/enterprise/enterprise-devops-at-scale.jpg
      alt: "Bild: Zusammenarbeit"
    items:
      - icon:
          name: increase
          alt: Vergrößern-Symbol
          variant: marketing
        header: Produktiver zusammenarbeiten
        text: Eliminieren Sie Click-Ops und führen Sie Check & Balances ein, die für die Übernahme in die Cloud unerlässlich sind.
      - icon:
          name: gitlab-release
          alt: GitLab Release-Symbol
          variant: marketing
        header: Risiken und Kosten reduzieren
        text: Mehr Tests, frühere Fehlererkennung, weniger Risiko.
      - icon:
          name: collaboration
          alt: Symbol:⁠Zusammenarbeit
          variant: marketing
        header: Bessere Software schneller bereitstellen
        text: Minimieren Sie sich wiederholende Aufgaben und konzentrieren Sie sich auf wertschöpfende Aufgaben.
      - icon:
          name: release
          alt: 'Symbol: Agil'
          variant: marketing
        header: DevSecOps vereinfachen
        text: Verwalten Sie alle Ihre DevSecOps-Prozesse an einem Ort und skalieren Sie so Ihren Erfolg, ohne die Komplexität zu erhöhen.
  by_industry_solutions_block:
    subtitle: Die komplette DevSecOps-Plattform für den öffentlichen Sektor
    sub_description: "Angefangen bei einer DevSecOps-Plattform, die eine sichere und robuste Quellcode-Verwaltung (SCM), kontinuierliche Integration (CI), kontinuierliche Bereitstellung (CD) sowie kontinuierliche Software-Sicherheit und Compliance umfasst, erfüllt GitLab individuelle Anforderungen:"
    white_bg: true
    sub_image: /nuxt-images/solutions/public-sector/showcase-pubsec.svg
    alt: 'Bild: Benefits'
    solutions:
      - title: Agile Planung
        description: Planen, initiieren, priorisieren und verwalten Sie Innovationsinitiativen, mit vollständiger Transparenz und Einbindung in die laufende Arbeit.
        icon:
          name: release
          alt: 'Symbol: Agil'
          variant: marketing
        link_text: Mehr erfahren
        link_url: /solutions/agile-delivery
        data_ga_name: agile planning
        data_ga_location: body
      - title: Automatisierte Software-bereitstellung
        description: Prüfen Sie die Leistungsbeschreibung der Software Ihres Projekts mit den wichtigsten Details zu den verwendeten Abhängigkeiten, einschließlich der bekannten Schwachstellen.
        icon:
          name: automated-code
          alt: 'Symbol: Automatisiertes Programmieren'
          variant: marketing
        link_text: Mehr erfahren
        link_url: /solutions/delivery-automation/
        data_ga_name: Automated Software Delivery
        data_ga_location: body
      - title: Kontinuierliche Sicherheit und Konformität
        description: Berücksichtigen Sie Sicherheitsaspekte von Anfang an und automatisieren Sie die Einhaltung der Konformität während des gesamten Entwicklungsprozesses, um Risiken und Verzögerungen zu reduzieren.
        icon:
          name: devsecops
          alt: 'Symbol: DevSecOps'
          variant: marketing
        link_text: Mehr erfahren
        link_url: /solutions/continuous-software-compliance/
        data_ga_name: Continuous Security & Compliance
        data_ga_location: body
      - title: Wertschöpfungs-management
        description: Bieten Sie allen Stakeholdern im Unternehmen wertvolle Einblicke in jede Phase der Ideenfindung und Entwicklung.
        icon:
          name: visibility
          alt: 'Symbol: Sichtbarkeit'
          variant: marketing
        link_text: Mehr erfahren
        link_url: /solutions/value-stream-management
        data_ga_name: Value Stream Management
        data_ga_location: body
      - title: Zuverlässigkeit
        description: Mit Geo können global verteilte Teams im Rahmen einer Notfallwiederherstellungsstrategie einen schnellen und effizienten Service mit einem Bereitschaftsdienst bereitstellen.
        icon:
          name: remote-world
          alt: 'Symbol: Remote-Welt'
          variant: marketing
      - title: Hochverfügbarkeit – im großen Stil
        description: Referenzarchitektur für Hochverfügbarkeit für über 50.000 Benutzer(innen)
        icon:
          name: auto-scale
          alt: 'Symbol: Automatische Skalierung'
          variant: marketing
        link_text: Mehr erfahren
        link_url: https://docs.gitlab.com/ee/administration/reference_architectures/
        data_ga_name: High availability
        data_ga_location: body
  by_solution_value_prop:
    title: Eine Plattform für Dev, Sec und Ops
    header_animation: fade-up
    header_animation_duration: 500
    cards_animation: zoom-in-up
    cards_animation_duration: 500
    cards:
      - title: Umfassend
        description: Visualisieren und optimieren Sie Ihren gesamten DevSecOps-Lebenszyklus mit plattformweiten Analysen innerhalb desselben Systems, in dem Sie Ihre Arbeit erledigen.
        icon:
          name: digital-transformation
          alt: 'Symbol: Digitale Transformation'
          variant: marketing
      - title: DevSecOps vereinfacht
        description: Verwenden Sie einheitliche Tools für alle Teams und Lebenszyklusphasen, ohne Abhängigkeiten von Plugins oder APIs von Drittanbietern, die Ihren Arbeitsablauf stören können.
        icon:
          name: devsecops
          alt: 'Symbol: DevSecOps'
          variant: marketing
      - title: Sicher
        description: Scannen Sie bei jedem Commit auf Schwachstellen und Konformitätsverletzungen.
        icon:
          name: eye-magnifying-glass
          alt: 'Symbol: Auge in Lupe'
          variant: marketing
      - title: Transparent und konform
        description: Erfassen und koordinieren Sie automatisch alle Aktionen – von der Planung über Codeänderungen bis hin zu Genehmigungen – für eine einfache Nachvollziehbarkeit bei Audits oder Retrospektiven.
        icon:
          name: release
          alt: 'Symbol: Schutzschild'
          variant: marketing
      - title: Einfach zu skalieren
        description: Referenzarchitekturen zeigen Ihnen, wie Sie Hochverfügbarkeit für Installationen mit mehr als 50.000 Benutzern skalieren können.
        icon:
          name: monitor-web-app
          alt: 'Symbol: Web-App-Überwachung'
          variant: marketing
      - title: Skalierbar
        description: Stellen Sie GitLab auf einem Kubernetes-Cluster bereit und skalieren Sie horizontal. Upgrades verursachen keine Ausfallzeiten. Verwenden Sie den GitOps-Workflow oder den CI/CD-Workflow.
        icon:
          name: auto-scale
          alt: 'Symbol: Automatische Skalierung'
          variant: marketing
  by_industry_case_studies:
    title: Realisierte Kundenvorteile
    link:
      text: Alle Fallstudien
    charcoal_bg: true
    header_animation: fade-up
    header_animation_duration: 500
    row_animation: fade-right
    row_animation_duration: 800
    rows:
      - title: Siemens
        subtitle: Siemens hat mit GitLab eine Open Source DevSecOps-Kultur geschaffen
        image:
          url: /nuxt-images/blogimages/siemenscoverimage_casestudy.jpg
          alt: Unternehmensmeeting
        button:
          href: /customers/siemens/
          text: Mehr erfahren
          data_ga_name: siemens learn more
          data_ga_location: body
      - title: Hilti
        subtitle: So konnten CI/CD und robuste Sicherheitsüberprüfungen den SDLC von Hilti beschleunigen
        image:
          url: /nuxt-images/blogimages/hilti_cover_image.jpg
          alt: Gebäude in der Ferne
        button:
          href: /customers/hilti/
          text: Mehr erfahren
          data_ga_name: hilti learn more
          data_ga_location: body
      - title: Bendigo
        subtitle: GitLab beschleunigt DevSecOps bei der Bendigo and Adelaide Bank
        image:
          url: /nuxt-images/blogimages/bab_cover_image.jpg
          alt: Gebäude in der Innenstadt
        button:
          href: /customers/bab/
          text: Mehr erfahren
          data_ga_name: bendigo learn more
          data_ga_location: body
      - title: Radio France
        subtitle: GitLab CI/CD beschleunigt die Bereitstellung bei Radio France um das 5-fache
        image:
          url: /nuxt-images/blogimages/radio-france-cover-image.jpg
          alt: Frankreich, Eiffelturm
        button:
          href: /customers/radiofrance/
          text: Mehr erfahren
          data_ga_name: radio france learn more
          data_ga_location: body
  solutions_resource_cards:
    column_size: 4
    title: Ressourcen
    link:
      text: Alle Ressourcen anzeigen
    cards:
      - icon:
          name: webcast
          alt: 'Symbol: Webcast'
          variant: marketing
        event_type: Webinar
        header: Liefern Sie mit einer End-to-End DevOps-Plattform mehr Wert mit weniger Aufwand
        link_text: Jetzt ansehen
        image: /nuxt-images/features/resources/resources_waves.png
        alt: Wellen
        href: https://www.youtube.com/watch?v=wChaqniv3HI
        aos_animation: fade-up
        aos_duration: 400
      - icon:
          name: webcast
          alt: 'Symbol: Webcast'
          variant: marketing
        event_type: Webinar
        header: Technische Demo der DevOps-Plattform
        link_text: Jetzt ansehen
        image: /nuxt-images/features/resources/resources_webcast.png
        alt: Arbeiten in einem Café
        href: https://youtu.be/Oei67XCnXMk
        aos_animation: fade-up
        aos_duration: 600
      - icon:
          name: event
          alt: 'Symbol: Event'
          variant: marketing
        event_type: Virtuelle Veranstaltung
        header: Die digitale Transformation von Northwestern Mutual mit GitLab
        link_text: Jetzt ansehen
        image: /nuxt-images/resources/fallback/img-fallback-cards-infinity.png
        alt: GitLab Unendlichkeitsschleife
        href: https://www.youtube.com/watch?v=o6EY_WwEFpE
        aos_animation: fade-up
        aos_duration: 800
      - icon:
          name: event
          alt: 'Symbol: Event'
          variant: marketing
        event_type: Virtuelle Veranstaltung
        header: Die nächste Iteration von DevOps (CEO Keynote)
        link_text: Jetzt ansehen
        image: /nuxt-images/resources/fallback/img-fallback-cards-devops.png
        alt: 'Bild: DevOps'
        href: https://www.youtube.com/watch?v=Wx8tDVSeidk
        aos_animation: fade-up
        aos_duration: 1000
      - icon:
          name: case-study
          alt: 'Symbol: Fallstudie'
          variant: marketing
        event_type: Case Study
        header: Goldman Sachs verbessert sich von einem Build alle zwei Wochen auf über eintausend pro Tag
        link_text: Mehr lesen
        image: /nuxt-images/features/resources/resources_case_study.png
        alt: Bäume von oben
        href: /customers/goldman-sachs/
        data_ga_name: Goldman Sachs
        data_ga_location: body
        aos_animation: fade-up
        aos_duration: 1200
      - icon:
          name: video
          alt: 'Symbol: Video'
          variant: marketing
        event_type: Video
        header: GitLab Infomercial
        link_text: Jetzt ansehen
        image: /nuxt-images/features/resources/resources_golden_dog.png
        alt: Schlafender Hund
        href: https://www.youtube.com/embed/gzYTZhJlHoI? (Link nur auf Englisch)
        aos_animation: fade-up
        aos_duration: 1400
