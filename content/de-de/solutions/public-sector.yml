---
  title: GitLab für den öffentlichen Sektor
  description: Social Coding, eine kontinuierliche Integration sowie Release-Automatisierung beschleunigen nachweislich die Entwicklung und Softwarequalität, um Ziele zu erreichen. Mehr erfahren!
  twitter_image: /nuxt-images/open-graph/gitlab-blog-cover.png
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab für den öffentlichen Sektor
        subtitle: Die DevSecOps-Plattform, mit der du deine Geschwindigkeit beschleunigen und deine Ziele erreichen kannst
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Kostenlose Testversion starten
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: Fragen? Kontakt
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/public-sector.jpg
          image_url_mobile: /nuxt-images/solutions/public-sector.jpg
          alt: "Bild: Gitlab für den öffentlichen Sektor"
          bordered: true
    - name: 'by-industry-intro'
      data:
        logos:
          - name: Logo der University of Washington
            image: /nuxt-images/logos/uw-logo.svg
            url: https://about.gitlab.com/customers/uw/
            aria_label: Link zur Kunden-Fallstudie der University of Washington
          - name: Logo von Lockheed Martin
            image: /nuxt-images/logos/lockheed-martin.png
            url: https://about.gitlab.com/customers/lockheed-martin/
            aria_label: Link zur Kunden-Fallstudie von Lockheed Martin
          - name: Logo von Cook County
            image: /nuxt-images/logos/cookcounty-logo.svg
            url: https://about.gitlab.com/customers/cook-county/
            aria_label: Link zur Kunden-Fallstudie von Cook County
          - name: Logo der University of Surrey
            image: /nuxt-images/case-study-logos/logo_uni_surrey.svg
            url: https://about.gitlab.com/customers/university-of-surrey/
            aria_label: Link zur Kunden-Fallstudie der University of Surrey
          - name: EAB-Logo
            image: /nuxt-images/logos/eab-logo.svg
            url: https://about.gitlab.com/customers/EAB/
            aria_label: Link zur E.A.B.-Kunden-Fallstudie
          - name: Logo der Victoria University Wellington
            image: /nuxt-images/logos/victoria-university-wellington-logo.svg
            url: https://about.gitlab.com/customers/victoria_university/
            aria_label: Link zur Kunden-Fallstudie der Victoria University Wellington
    - name: 'side-navigation-variant'
      links:
        - title: Übersicht
          href: '#overview'
        - title: Kundenstimmen
          href: '#testimonials'
        - title: Funktionen
          href: '#capabilities'
        - title: Vorteile
          href: '#benefits'
        - title: Fallstudien
          href: '#case-studies'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: Sicherheit. Effizienz. Kontrolle.
                image:
                  image_url: "/nuxt-images/solutions/benefits/by-solution-benefits-public-sector.jpeg"
                  alt: Bild von Zusammenarbeit
                is_accordion: true
                items:
                  - icon:
                      name: devsecops
                      alt: DevSecOps-Symbol
                      variant: marketing
                    header: Verringern von Sicherheits- und Konformitätsrisiken
                    text: Erkenne Sicherheits- und Konformitätsfehler frühzeitig im Prozess und setze gleichzeitig konsistente Leitlinien im gesamten DevSecOps-Lebenszyklus durch.
                    link_text: Erfahre mehr über DevSecOps
                    link_url: /solutions/security-compliance/
                    ga_name: reduce security learn more
                    ga_location: benefits
                  - icon:
                      name: repo-code
                      alt: Repository mit Code-Symbol
                      variant: marketing
                    header: Ressourcen freigeben
                    text: Verbesserte Erfahrung für Normalbenutzer und Eindämmung globaler Bedrohungen durch Beseitigung anfälliger und komplexer DIY-Toolchains, die Zusammenarbeit und Innovation behindern.
                    link_text: Warum GitLab
                    link_url: /why-gitlab/
                    ga_name: free up learn more
                    ga_location: benefits
                  - icon:
                      name: digital-transformation
                      alt: Symbol für digitale Transformation
                      variant: marketing
                    header: Modernisiere und Vereinfachen auf einen Schlag
                    text: Verwende die DevSecOps-Plattform, die auf die einzigartigen Anforderungen von Cloud-nativen Anwendungen und der Infrastruktur, auf die diese sich verlassen, zugeschnitten ist.
                    link_text: Erfahre mehr über unseren Plattform-Ansatz
                    link_url: /solutions/devops-platform/
        - name: 'div'
          id: 'testimonials'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              data:
                header: |
                  Die Regierung vertraut uns.
                  <br />
                  Entwickler lieben uns.
                quotes:
                  - title_img:
                      url: /nuxt-images/logos/usarmy.svg
                      alt: US Army
                    quote: Anstatt Menschen 10 verschiedene Dinge lehren zu müssen, lernen sie nur noch eine Sache, was auf lange Sicht viel einfacher ist.
                    author: Chris Apsey
                    position: Captain, U.S. Army
                    ga_carousel: public-sector us army
                  - title_img:
                      url: /nuxt-images/logos/uw-logo.svg
                      alt: Logo der University of Washington
                    quote: In den letzten zwei Jahren hat GitLab die Organisation hier an der UW transformiert. Die Plattform ist fantastisch!
                    author: Aaron Timss
                    position: Director of Information Technology, CSE
                    ga_carousel: public-sector university of washington
                  - title_img:
                      url: /nuxt-images/logos/cookcounty-logo.svg
                      alt: Logo von Cook County
                    quote: Mit GitLab kann ich Forschungsarbeiten erstellen und die Ergebnisse mit anderen Personen im Büro teilen. Diese können Änderungen an der Arbeit vorschlagen und ich kann diese Änderungen übernehmen, ohne mich um die Versionskontrolle und das Speichern meiner Arbeit kümmern zu müssen. Ein Repository sorgt dafür, dass ich mich mehr auf die eigentliche Arbeit und weniger auf die Arbeitsabläufe konzentrieren kann.
                    author: Robert Ross
                    position: Chief Data Officer
                    ga_carousel: public-sector cook county
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              data:
                subtitle: Die umfassendste DevSecOps-Plattform für den öffentlichen Sektor
                sub_description: 'Mithilfe der DevSecOps-Plattform, die ein sicheres und robustes Quellcode-Management (SCM), kontinuierliche Integration (CI), kontinuierliche Lieferung (CD) sowie kontinuierliche Software-Sicherheit und Konformität bietet, erfüllt GitLab individuelle Anforderungen, wie zum Beispiel:'
                white_bg: true
                sub_image: /nuxt-images/solutions/public-sector/showcase-pubsec.svg
                alt: "Bild: Vorteile"
                solutions:
                  - title: SBOM
                    description: Überprüfe die Leistungsbeschreibung der Software deines Projekts mit wichtigen Details zu den verwendeten Abhängigkeiten, einschließlich bekannter Schwachstellen.
                    icon:
                      name: less-risk
                      alt: "Symbol: Weniger Risiko"
                      variant: marketing
                    link_text: Mehr erfahren
                    link_url: https://docs.gitlab.com/ee/user/application_security/dependency_list/
                    data_ga_name: sbom
                    data_ga_location: solutions block
                  - title: Zero Trust
                    description: Erfahre, wie GitLab die Prinzipien von Zero Trust befolgt und Best Practices demonstriert.
                    icon:
                      name: monitor-pipeline
                      alt: Monitor-Pipeline-Symbol
                      variant: marketing
                    link_text: Mehr erfahren
                    link_url: /handbook/security/
                    data_ga_name: zero trust
                    data_ga_location: solutions block
                  - title: Schwachstellenmanagement
                    description: Verwalte Software-Schwachstellen zentral an einem Ort – innerhalb der Pipeline, für einzelne Projekte, für Gruppen von Projekten sowie gruppenübergreifend.
                    icon:
                      name: shield-check
                      alt: Schutzschild-Symbol mit Häkchen
                      variant: marketing
                    link_text: Mehr erfahren
                    link_url: https://docs.gitlab.com/ee/user/application_security/vulnerability_report/
                    data_ga_name: vulnerability management
                    data_ga_location: solutions block
                  - title: Fuzz-Test
                    description: Mit GitLab kannst du deinen Pipelines nicht nur eine umfassende Reihe von Scannern hinzufügen, sondern auch Fuzz-Tests. Fuzz-Tests senden zufällige Eingaben an eine instrumentierte Version deiner Anwendung, um unerwartetes Verhalten zu verursachen. Dieses Verhalten weist auf Sicherheits- und Logikfehler hin, die behoben werden sollten.
                    icon:
                      name: monitor-test
                      alt: Monitor-Test-Symbol
                      variant: marketing
                    link_text: Mehr erfahren
                    link_url: https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/
                    data_ga_name: fuzz testing
                    data_ga_location: solutions block
                  - title: Offline-Umgebungen
                    description: Selbst wenn die Verbindung zum Internet getrennt ist, kannst du die meisten GitLab-Sicherheitsscanner ausführen.
                    icon:
                      name: gitlab-monitor-alt
                      alt: Monitor-GitLab-Symbol
                      variant: marketing
                    link_text: Mehr erfahren
                    link_url: https://docs.gitlab.com/ee/user/application_security/offline_deployments/
                    data_ga_name: offline environment
                    data_ga_location: solutions block
                  - title: Allgemeine Konformitätskontrollen
                    description: Automatisiere und setze allgemeine Richtlinien durch, wie beispielsweise Aufgabentrennung, geschützte Branches und Push-Regeln.
                    icon:
                      name: shield-check
                      alt: Schutzschild-Symbol mit Häkchen
                      variant: marketing
                    link_text: Mehr erfahren
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html
                    data_ga_name: common controls for compliance
                    data_ga_location: solutions block
                  - title: Konformitäts-Pipelines
                    description: Erzwinge Pipeline-Scan-Konfigurationen, um sicherzustellen, dass obligatorische Sicherheitsscans nicht umgangen werden.
                    icon:
                      name: shield-check
                      alt: Schutzschild-Symbol mit Häkchen
                      variant: marketing
                    link_text: Mehr erfahren
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html
                    data_ga_name: compliance
                    data_ga_location: solutions block
                  - title: Niedrige bis hohe Entwicklungsstufe
                    description: Vereinfache die Zusammenarbeit zwischen verschiedenen Entwicklungsteams.
                    icon:
                      name: cog-code
                      alt: Zahnrad-Symbol mit Code
                      variant: marketing
                    link_text: Mehr erfahren
                    link_url: https://www.youtube.com/watch?v=HSfDTslLRT8/
                    data_ga_name: low to high
                    data_ga_location: solutions block
                  - title: Lokal, selbst gehostet oder SaaS
                    description: GitLab funktioniert in allen Umgebungen. Du hast die Wahl.
                    icon:
                      name: gitlab-monitor-alt
                      alt: Monitor-GitLab-Symbol
                      variant: marketing
                    link_text: Mehr erfahren
                    link_url: /pricing/
                    data_ga_name: on-prem self-hosted or saas
                    data_ga_location: solutions block
                  - title: Gehärtetes Container-Image
                    description: Das DoD-konforme, gehärtete Container-Image minimiert das Risikoprofil, ermöglicht die schnelle Bereitstellung sichererer Anwendungen und unterstützt die kontinuierliche Autorität zum Betrieb von Prozessen. Diese Images werden auch in der Iron Bank akzeptiert.
                    icon:
                      name: lock-cog
                      alt: Zahnrad-Symbol im Vorhängeschloss
                      variant: marketing
                    link_text: Mehr erfahren
                    link_url: /press/releases/2020-07-01-gitlab-announces-hardened-container-image-in-support-of-the-us-department-of-defense-enterprise-devsecops-initiative.html
                    data_ga_name: hardened container image
                    data_ga_location: solutions block
        - name: 'div'
          id: 'benefits'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-value-prop'
              data:
                title: Perfekt für den öffentlichen Sektor
                cards:
                  - title: NIST SSDF
                    description: GitLab orientiert sich an den NIST-Leitlinien und unterstützt CIOs bei der Umsetzung der erforderlichen Maßnahmen zum Schutz der Software-Lieferkette, um ihre Unternehmen proaktiv zu verteidigen. Erfahre mehr darüber, wie <a href="/blog/2022/03/29/comply-with-nist-secure-supply-chain-framework-with-gitlab/" data-ga-name="gitlab meets nist" data-ga-location="value prop">GitLab die NIST-Richtlinien SSDF 1.1 erfüllt</a>.
                    icon:
                      name: less-risk
                      alt: "Symbol: Weniger Risiko"
                      variant: marketing
                  - title: Die Alternative zu DI2E
                    description: Der Zugriff auf DI2E (Defense Intelligence Information Enterprise) wurde eingestellt – dies zwingt Behörden dazu, ihr gesamtes DevSecOps-Modell zu überdenken. GitLab ist eine solide Alternative zu DI2E und unsere Einzelanwendung vereinfacht die Beschaffung.
                    icon:
                      name: devsecops
                      alt: DevSecOps-Symbol
                      variant: marketing
                  - title: Transparenz und Kontrolle für die Lieferkette
                    description: Die DevSecOps-Plattform von GitLab wird als eine einzige, <a href="/press/releases/2020-07-01-gitlab-announces-hardened-container-image-in-support-of-the-us-department-of-defense-enterprise-devsecops-initiative.html" data-ga-name="hardened" data-ga-location="value prop">gehärtete</a> Anwendung bereitgestellt, was die durchgängige Transparenz und Rückverfolgbarkeit vereinfacht. Sicherheits- und Konformitätsrichtlinien werden in allen DevSecOps-Prozessen einheitlich verwaltet und durchgesetzt.
                    icon:
                      name: eye-magnifying-glass
                      alt: Lupen-Symbol mit Auge
                      variant: marketing
                  - title: Lokal, selbst gehostet oder SaaS
                    description: Du hast die Wahl.
                    icon:
                      name: monitor-web-app
                      alt: Monitor-Web-App-Symbol
                      variant: marketing
        - name: 'div'
          id: 'case-studies'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-case-studies'
              data:
                title: Kundennutzen
                link:
                  text: Alle Fallstudien
                  link: /customers/
                rows:
                  - title: U.S. Army Cyber School
                    subtitle: Wie die U.S. Army Cyber School „Courseware as Code“ mit GitLab erstellt hat
                    image:
                      url: /nuxt-images/blogimages/us-army-cyber-school.jpeg
                      alt: Soldat spricht über Funk
                    button:
                      href: /customers/us_army_cyber_school/
                      text: Mehr erfahren
                      data_ga_name: us army learn more
                      data_ga_location: case studies
                  - title: Cook County Assessor's Office
                    subtitle: Wie Cook County in Chicago Wirtschaftsdaten mit Transparenz und Versionskontrolle auswertet
                    image:
                      url: /nuxt-images/blogimages/cookcounty.jpg
                      alt: Häuser aus Vogelperspektive
                    button:
                      href: /customers/cook-county/
                      text: Mehr erfahren
                      data_ga_name: cook country learn more
                      data_ga_location: case studies
                  - title: University of Washington
                    subtitle: Das Paul G. Allen Center for Computer Science & Engineering gewinnt an Kontrolle und Flexibilität und verwaltet mühelos über 10.000 Projekte.
                    image:
                      url: /nuxt-images/blogimages/uw-case-study-image.png
                      alt: Campus der University of Washington
                    button:
                      href: /customers/uw/
                      text: Mehr erfahren
                      data_ga_name: uw learn more
                      data_ga_location: case studies
    - name: 'by-solution-link'
      data:
        title: 'GitLab-Events'
        description: "Nimm an Veranstaltungen teil, um zu erfahren, wie dein Team Software schneller und effizienter bereitstellen und gleichzeitig Sicherheit sowie Konformität stärken kann. Wir werden im 2022 zahlreiche Veranstaltungen besuchen und veranstalten. Wir können es kaum erwarten, dich dort zu sehen!"
        link: /events/
        button_text: Erfahren Sie mehr
        image: /nuxt-images/events/google-cloud-next/pubsec-event-link.jpeg
        alt: Menschenmenge bei einer Präsentation
        icon: calendar-alt-2
