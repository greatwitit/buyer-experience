# This file is not directly used, but is used as a template for new competition pages.
---
  data:
    competitor: Azure DevOps
    competitor_product: Azure DevOps
    gitlab_coverage: 75
    competitor_coverage: 50
    subheading: # Brief text underneath the heading.
    comparison_table:
      - stage: Verify
        features:
          - feature: Continuous Integration (CI) - Pipelines
            gitlab:
              coverage: 100
              projected_coverage: 100
              description: GitLab is a leading edge tool in CI and provides and maintains all the features required for a complete continuous integration solution.
              details: |
                * Customers can easily self-manage Runners on VM's, containers, or physical machines in the cloud or on-premise and use those at the group or project level on GitLab SaaS to run CI jobs
                * A directed acyclic graph can be used in the context of a CI/CD pipeline to build relationships between jobs such that execution is performed in the quickest possible manner, regardless how stages may be set up.
                * Similarly to multi-project pipelines, a pipeline can trigger a set of concurrently running downstream child pipelines, but in the same project: |
                    * Child pipelines still execute each of their jobs according to a stage sequence, but would be free to continue forward through their stages without waiting for unrelated jobs in the parent pipeline to finish.
                    * The configuration is split up into smaller child pipeline configurations. Each child pipeline contains only relevant steps which are easier to understand. This reduces the cognitive load to understand the overall configuration.
                    * Imports are done at the child
                * Use resource groups to strategically control the concurrency of the jobs for optimizing continuous deployments workflow with safety.
                * Use scheduled pipelines to run GitLab CI/CD pipelines at regular intervals.
              improving_product_capabilities: |
                * [Secure CI_JOB_TOKEN Workflows](https://gitlab.com/groups/gitlab-org/-/epics/6546).
                * [Make GitLab CI/CD work better with external repositories](https://gitlab.com/groups/gitlab-org/-/epics/943).
              link_to_documentation: https://docs.gitlab.com/ee/ci/
            competitor:
              coverage: 75
              description: With Azure, users can build, test and deploy apps. Azure makes it easier to work if you use it for Microsoft technologies (.net, Azure Cloud, etc)
              details: |
                * Marketplace with a wide range of community-built build, test, and deployment tasks, along with hundreds of extensions from Slack to SonarCloud.
                * Supports Microsoft-hosted agents and Self-hosted agents (runners)
                * Integrates with Azure Deployments and with GitHub
                * Supports for builds on Linux, Windows, and macOS.
                * YAML pipeline editor with Intellisense support
                * The lack of built-in pipeline validation requires users to commit changes to validate or use a third party extension. GitLab provides this functionality built-in in the Pipeline Editor which saves time and resources.

              link_to_documentation: https://learn.microsoft.com/en-us/azure/devops/pipelines/?view=azure-devops
          - feature: Secrets Management
            gitlab:
              coverage: 25
              projected_coverage: 50
              description: GitLab provides basic functionality for secret management by allowing secret variables to be set at the project/group levels and limits access.
              details: |
                * CI/CD variables can be added to a project’s settings. Only project members with the Maintainer role can add or update project CI/CD variables. To keep a CI/CD variable secret, put it in the project settings.
                  * Protect variable: If selected, the variable is only available in pipelines that run on protected branches or protected tags.
                  * Mask variable: If selected, the variable’s Value is masked in job logs. The variable fails to save if the value does not meet the masking requirements.
                * Files can be stored securely for use in CI/CD pipelines as “secure files”. These files are stored securely outside of a project’s repository, and are not version controlled. It is safe to store sensitive information in these files. Secure files support both plain text and binary file types.
                * GitLab has built in integration with Vault, however this means that the secrets must be created and managed using Vault.
                * Variable values are encrypted using aes-256-cbc and stored in the database. This data can only be read and decrypted with a valid secrets file.
              improving_product_capabilities: |
                * Increase in Paid GMAU for gitlab.com - by making our [JWT token support OpenIDC](https://gitlab.com/groups/gitlab-org/-/epics/7335) which allows us to integration with additional cloud providers
                * [Extend our lead in CI/CD](https://about.gitlab.com/direction/#extend-our-lead-in-cicd):
                  * [Support non-expanded variables](https://gitlab.com/gitlab-org/gitlab/-/issues/217309) which will solve one of the popular [requests](https://gitlab.com/gitlab-org/gitlab/-/issues/17069) for secrets management
                  * Allow users to [Opt-in JWT token](https://gitlab.com/gitlab-org/gitlab/-/issues/356986) per job which improved our securities around key CI/CD workflows
                * [Support $ sign in environment variables](https://gitlab.com/gitlab-org/gitlab/-/issues/17069)
              link_to_documentation: https://docs.gitlab.com/ee/ci/variables/
            competitor:
              coverage: 25
              description: Azure DevOps provides basic functionality to store secrets using variables, or link group variables to Azure Key Vault.
              details: |
                * Set secret variables in the UI
                * Secret variables are encrypted at rest with a 2048-bit RSA key and are available on the agent for tasks and scripts to use.
                * You can [link](https://learn.microsoft.com/en-us/azure/devops/pipelines/process/set-secret-variables?view=azure-devops&tabs=yaml%2Cbash#link-secrets-from-an-azure-key-vault) an existing [Azure key vault](https://azure.microsoft.com/en-us/products/key-vault) to a variable group

              link_to_documentation: https://learn.microsoft.com/en-us/azure/devops/pipelines/process/set-secret-variables?view=azure-devops&tabs=yaml%2Cbash
          - feature: Code Testing and Coverage
            gitlab:
              coverage: 50
              projected_coverage: 50
              description: GitLab offers many tools for visualizing test cases as well as determining coverage and failure.
              details: |
                * Test cases in GitLab can help teams create testing scenarios in their existing development platform.
                * With the help of GitLab CI/CD, users can collect the test coverage information of their favorite testing or coverage-analysis tool, and visualize this information inside the file diff view of merge requests (MRs). This will allow users to see which lines are covered by tests, and which lines still require coverage, before the MR is merged.
                * Users can configure their job to use Unit test reports, and GitLab displays a report on the merge request so that it’s easier and faster to identify the failure without having to check the entire log.
                * Unit test reports currently only support test reports in the JUnit report format.
              improving_product_capabilities: |
                * [Improve aggregation of code coverage across jobs](https://gitlab.com/gitlab-org/gitlab/-/issues/367317)
                * [Add support for Jacoco Coverage Report Parsing](https://gitlab.com/groups/gitlab-org/-/epics/6900)
                * [Historic Test Data for Projects](https://gitlab.com/groups/gitlab-org/-/epics/3129)
              link_to_documentation: https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html
            competitor:
              coverage: 75
              description: Azure pipelines provides advanced test reporting capabilities such as test results analytics, code coverage report and pull request code coverage.
              details: |
                * Test report available in the [test tab, build, release](https://learn.microsoft.com/en-us/azure/devops/pipelines/test/review-continuous-test-results-after-build?view=azure-devops)
                * Test analytics shows you the pass tests rate, top failing tests, etc, and you can have the same info for Release.
                * Provides code full coverage for the entire codebase of a project , and diff coverage for the context of a pull request only.
                * Coverage indicators annotate lines that are changed to show whether those lines are covered.

              link_to_documentation: https://learn.microsoft.com/en-us/azure/devops/pipelines/test/review-continuous-test-results-after-build?view=azure-devops
          - feature: Merge Trains
            gitlab:
              coverage: 50
              projected_coverage: 100
              description: GitLab has Merge trains enabled under Premium.
              details: |
                * A merge train is a queued list of merge requests, each waiting to be merged into the target branch.
                * Many merge requests can be added to the train. Each merge request runs its own merged results pipeline, which includes the changes from all of the other merge requests in front of it on the train. All the pipelines run in parallel, to save time. The author of the internal merged result commit is always the user that initiated the merge.
              improving_product_capabilities: |
                * [Merge Trains should support fast forward merge](https://gitlab.com/groups/gitlab-org/-/epics/4911)
                * [API support for merge trains MWPS](https://gitlab.com/gitlab-org/gitlab/-/issues/32665)
                * [Resolving Severity 1 and 2 bugs](https://gitlab.com/gitlab-org/gitlab/-/issues?sort=milestone_due_desc&state=opened&label_name[]=Category:Merge+Trains&label_name[]=type::bug&not[label_name][]=severity::3&not[label_name][]=severity::4)
              link_to_documentation: https://docs.gitlab.com/ee/ci/
            competitor:
              coverage: 50
              description: Azure pipelines provides build validation
              details: A build validation policy queues a new build when a new PR is created or changes are pushed to an existing PR that targets the branch. The build policy evaluates the build results to determine whether the PR can be completed.
              link_to_documentation: https://learn.microsoft.com/en-us/azure/devops/repos/git/branch-policies?view=azure-devops&tabs=browser#build-validation
          - feature: Review Apps
            gitlab:
              coverage: 100
              projected_coverage: 100
              description: GitLab allows users to create a new environment for each branch, and view changes in a live deployment.
              details: |
                * Provide an automatic live preview of changes made in a feature branch by spinning up a dynamic environment for merge requests.
                * Allow designers and product managers to see changes without needing to check out a branch and run the changes in a sandbox environment.
                * Are fully integrated with the GitLab DevOps LifeCycle.
                * Allow users to deploy their changes wherever they want.
                * Users can leave comments directly to the MR the review app was started from as well
                * What happens when 2 MRs and deployed to the same review app
                  * Users can always set a variable based off an env value of the environment to avoid collision
                * Works with the Kubernetes auto-integration
              improving_product_capabilities: |
                - [Using Review Apps for mobile](https://gitlab.com/groups/gitlab-org/-/epics/2372){data-ga-name="Using Review Apps for mobile" data-ga-location="review apps feature modal"}
                - [Group-level Review Apps](https://gitlab.com/gitlab-org/gitlab/-/issues/325759){data-ga-name="Group-level Review Apps]" data-ga-location="review apps feature modal"}
                - [Per commit Review Apps](https://gitlab.com/gitlab-org/gitlab/-/issues/17432){data-ga-name="Per commit Review Apps" data-ga-location="review apps feature modal"}
              link_to_documentation: https://docs.gitlab.com/ee/ci/
            competitor:
              coverage: 0
        overview_analysis: |
          Azure DevOps is mainly useful for companies that primarily use Microsoft’s .NET technology stack to develop and run their software applications, or which deploy to Azure only, because it firmly binds the user to the entire Microsoft ecosystem. However, when used for multicloud deployments or to build non-Microsoft based applications, such as Java, Ruby or even Python, users frequently report finding it not as seamless as GitLab. Microsoft will likely always make software development easier with Azure.

          Azure Pipelines can be integrated with other Azure services, such as Azure Boards, Azure Repos, etc,  but may have fewer integration options with external tools than GitLab or other CI/CD tools.

          Effective December 31, 2022, the Microsoft Security Code Analysis (MSCA) extension has been retired and replaced by Microsoft Security DevOps Azure DevOps extension. Microsoft Security DevOps is a command line application that integrates static analysis (and open source) tools into the development lifecycle. In GitLab, users get all scanning already configured with reports automatically appearing at the Merge Request, Security and Compliance Dashboard is available out of the box with zero configuration. Azure DevOps has no built-in tools for application security.

          Azure Pipelines will have additional costs for the user when attempting to complete the software development lifecycle, as third party components (such as security scans) will be required. Azure DevOps does not provide an end-to-end DevOps pipeline with zero configuration. Azure DevOps also does not have any out of the box release methodologies (canary, incremental, etc). Meanwhile, with GitLab - as the most comprehensive DevSecOps platform - the total cost is transparent and knowable, with no additional costs required.
        gitlab_product_roadmap:
          - roadmap_item: Make GitLab CI/CD work better with external repositories
          - roadmap_item: Merge Trains will support fast forward merge
          - roadmap_item: Extend our lead in CI/CD with support non-expanded variables
