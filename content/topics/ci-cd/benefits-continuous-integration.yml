---
  title: What is continuous integration (CI)?
  description: Learn about continuous integration and its benefits and see why organizations that use continuous integration have a competitive advantage over those that don’t.
  date_published: 2023-03-17
  date_modified: 2023-03-30
  topics_header:
    data:
      title: What is continuous integration (CI)?
      block:
        - metadata:
            id_tag: benefits-continuous-integration
          text: |
            Learn about continuous integration and its benefits and see why organizations that use continuous integration have a competitive advantage over those that don’t.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: CI/CD
      href: /topics/ci-cd/
      data-ga-name: ci-cd
      data_ga_location: breadcrumb
    - title: Benefits of continuous integration
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: Continuous integration (CI) explained
          href: "#continuous-integration-ci-explained"
          data_ga_name: ci explained
          data_ga_location: side-navigation
          variant: primary
        - text: What are the benefits of continuous integration for DevOps teams?
          href: "#what-are-the-benefits-of-continuous-integration-for-dev-ops-teams"
          data_ga_name: benefits of continuous integration for devops teams
          data_ga_location: side-navigation
        - text: The business benefits of continuous integration
          href: "#the-business-benefits-of-continuous-integration"
          data_ga_name: business benefits of continuous integration implementation
          data_ga_location: side-navigation
        - text: How to set up continuous integration
          href: "#how-to-set-up-continuous-integration"
          data_ga_name: how to set up continuous integration
          data_ga_location: side-navigation
        - text: Continuous integration (CI) vs. continuous delivery (CD)
          href: "#continuous-integration-ci-vs-continuous-delivery-cd"
          data_ga_name: continuous integration vs continuous delivery
          data_ga_location: side-navigation
    hyperlinks:
      text: "More on this topic"
      data:
        - text: Continuous integration pipelines
          href: /topics/ci-cd/cicd-pipeline/
          data_ga_location: side-navigation
          data_ga_name: Continuous integration pipelines
          variant: tertiary
          icon: true
        - text: Implement continuous integration
          href: /topics/ci-cd/implement-continuous-integration/
          data_ga_location: side-navigation
          data_ga_name: Implement continuous integration
          variant: tertiary
          icon: true
        - text: Continuous integration best practices
          href: /topics/ci-cd/continuous-integration-best-practices/
          data_ga_location: side-navigation
          data_ga_name: Continuous integration best practice
          variant: tertiary
          icon: true
    content:
      - name: topics-copy-block
        data:
          header: Continuous integration (CI) explained
          column_size: 10
          blocks:
            - text: |
                [Continuous integration, or CI](/topics/ci-cd/){data-ga-name="ci" data-ga-location="body"}, is the practice of integrating all your code changes into the main branch of a shared source code repository early and often, automatically testing each change when you commit or merge them, and automatically kicking off a build. CI helps DevOps teams detect and resolve conflicts early and ensures that the codebase remains stable. CI is a key practice for Agile development teams.

                The ultimate goal of CI is to deliver working code quickly and securely. Before you get started, there are two things you should keep in mind:

                **First, deliver code in small iterations**. Most software projects, even in small organizations, will involve a number of features that are being worked on by different team members. Even in the best case scenario, it can be difficult for team members to have visibility into what others are working on.

                This gets even worse if developers work on separate feature branches in isolation, and only merge to the main branch when their work is complete — and when the time comes to merge everyone’s work, everything from code conflicts to unexpected security concerns will delay the release. But if each developer pushes their updates to the main branch a little bit at a time, the team will be well on the way to achieving CI, with fewer conflicts and more predictable releases. Software development processes such as feature flags can help teams deliver new features to users quickly and safely.

                **Second, set up automated testing to keep your code safe and secure**. Long ago, the “build” and “test” phases of software development existed in isolation, with code being checked for security vulnerabilities only after it was complete and ready for release. An essential part of CI is continuous testing — testing the code for vulnerabilities throughout the development process. But as you might guess, this might be difficult to achieve manually. That’s where automated testing comes in. Today’s CI tools take the code pushed by each developer and run tests, such as unit tests or integration tests, automatically.
      - name: topics-copy-block
        data:
          header: What are the benefits of continuous integration for DevOps teams?
          column_size: 10
          blocks:
            - text: |
                CI makes software development easier, faster, and less risky for developers. By automating builds and tests, developers can make smaller changes and commit them with confidence. Software developers get feedback on their code sooner, increasing the overall pace of innovation.

                Organizations that adopt continuous integration have [a competitive advantage](/blog/2019/06/27/positive-outcomes-ci-cd/){data-ga-name="competitive advantage" data-ga-location="body"} because they can deploy faster. Organizations that have implemented CI are making revenue on the features they deploy, not waiting for manual code checks.

                Studies done by DevOps Research and Assessment (DORA) have shown that robust DevOps practices lead to improved [business outcomes](https://cloud.google.com/devops/state-of-devops/). All of these DORA 4 metrics can be improved by using CI:

                - **Lead time**: Early feedback and build/test automation help decrease the time it takes to go from code committed to code successfully running in production.
                - **Deployment frequency**: Automated tests and builds are a prerequisite to automated deploy.
                - **Time to restore service**: Automated pipelines enable fixes to be deployed to production faster, reducing mean time to resolution (MTTR).
                - **Change failure rate**: Early automated testing greatly reduces the number of defects that make their way to production.

      - name: topics-copy-block
        data:
          header: The business benefits of continuous integration
          column_size: 10
          blocks:
            - text: |
                By eliminating manual tasks, DevOps teams can work more efficiently and with greater speed. An automated workflow also improves handoffs, which enhances overall operational efficiency. The business benefits of continuous integration allow organizations to:

                - **Iterate faster**: Smaller code changes allow software development teams to iterate faster and are easier to manage.
                - **Find problems easily**: Teams can find problems in code because all code is managed and tested in smaller batches.Find problems easily: Teams can find problems in code because all code is managed and tested in smaller batches.
                - **Improve transparency**: Continuous feedback through frequent testing helps developers see where bugs are.
                - **Reduce costs**: Automated testing frees up time for developers by reducing manual tasks, and better code quality means fewer errors and less downtime.
                - **Make users happy**: Fewer bugs and errors make it into production, so users have a better experience.

                Automated testing reduces the chances of human error and ensures that only code that meets certain standards makes it into production. Because code is tested in smaller batches, there’s less context-switching for developers when a bug or error occurs. Pipelines can also identify where the error occurs, making it easier to not only identify problems, but fix them.

                A dev environment with fewer manual tasks means that engineers can spend more time on revenue-generating projects. With fewer errors, teams are more efficient and spend less time putting out fires. When processes such as unit testing are automated, engineers are happier and can focus on where they add the most value.

      - name: topics-copy-block
        data:
          header: How to set up continuous integration
          column_size: 10
          blocks:
            - text: |
                To set up CI for your project, here are some steps to follow:

                First, [choose a version control and CI tool](/solutions/continuous-integration/){data-ga-name="ci tool" data-ga-location="body"} that allows developers to push frequently and test continuously while reducing context switching.

                Then set up a shared central repository for your code and configure your CI tool to monitor your repository for changes and automatically build and test your code each time a change is detected.

                Finally, configure your CI tool to automatically deploy your code to a staging or production environment if the tests pass.

                Once you’ve set up CI, your team can focus on writing code and committing changes to the shared repository. The CI tool will handle the rest, automatically building, testing, and deploying your code. This can save your team time and reduce the risk of errors in your software.

      - name: topics-copy-block
        data:
          header: Continuous integration (CI) vs. continuous delivery (CD)
          column_size: 10
          blocks:
            - text: |
                CI goes hand in hand with continuous delivery, or CD. Together, [continuous integration and delivery (CI/CD)](/topics/ci-cd/){data-ga-name="ci-cd" data-ga-location="body"} bring [automation into the DevOps lifecycle](/blog/2019/04/25/5-teams-that-made-the-switch-to-gitlab-ci-cd/){data-ga-name="devops lifecycle automation" data-ga-location="body"}. Organizations that implement CI/CD make better use of their resources, are more cost efficient, and allow developers to focus on innovation.

                There are a few key differences between CI and CD. For one, CI generally happens more frequently than CD. CI is also typically used to refer to the process of automating the build and testing of code changes, while CD generally refers to the process of automating the release of code changes.

                - **Continuous integration (CI)** is the practice of merging all developer working copies to a shared mainline several times a day.
                - **Continuous delivery (CD)** is a software development practice where code changes are automatically built, tested, and deployed to production.
      - name: topics-cta
        data:
          subtitle: Continuous Integration (CI) with GitLab
          text: |
            Powerful automation to build and test faster at any scale
          column_size: 10
          cta_one:
            text: Learn More
            link: /stages-devops-lifecycle/continuous-integration/
            data_ga_name: Learn More
            data_ga_location: body
  components:
    - name: solutions-resource-cards
      data:
        title: More on continuous integration
        column_size: 6
        cards:
          - icon:
              name: case-study
              variant: marketing
              alt: Case Study Icon
            event_type: "Case study"
            header: How Hotjar deploys 50% faster with GitLab
            image: "/nuxt-images/resources/resources_7.jpeg"
            link_text: "Learn more"
            href: /customers/hotjar/
            data_ga_name: Hotjar deploys faster with GitLab
            data_ga_location: body
          - icon:
              name: ebook
              variant: marketing
              alt: Ebook Icon
            event_type: "Book"
            header: The benefits of a single application CI/CD
            image: "/nuxt-images/resources/resources_5.jpeg"
            link_text: "Learn more"
            href: /resources/ebook-single-app-cicd/
            data_ga_name: benefits of a single application
            data_ga_location: body

