---
  title: "New York World Tour"
  og_title: New York World Tour
  description: Join us at the GitLab DevSecOps World Tour in New York where we’ll be showcasing a comprehensive overview of the evolving DevSecOps landscape.
  og_description: Join us at the GitLab DevSecOps World Tour in New York where we’ll be showcasing a comprehensive overview of the evolving DevSecOps landscape.
  twitter_description: Join us at the GitLab DevSecOps World Tour in New York where we’ll be showcasing a comprehensive overview of the evolving DevSecOps landscape.
  og_image: /nuxt-images/events/world-tour/cities/new-york-illustration.png
  twitter_image: /nuxt-images/events/world-tour/cities/new-york-illustration.png
  time_zone: Eastern Time (ET)
  hero:
    breadcrumbs:
      - title: DevSecOps World Tour
        href: /events/devsecops-world-tour/
      - title: New York
    header: New York
    subtitle: October 12, 2023
    location: |
      Jay Suites Bryant Park
      109 W 39th St. 2nd Floor
      New York, NY 10018
    image:
      src: /nuxt-images/events/world-tour/cities/new-york-illustration.png
    button:
      text: Register for New York
      href: '#registration-form'
      variant: secondary
      icon:
        name: arrow-down
        variant: product
        size: sm
  agenda:
    - time: 9:30 am
      name: Registration & Breakfast
      speakers:
    - time: 10:00 am
      name: |
        Welcome Keynote: GitLab DevSecOps World Tour - The Evolution of DevSecOps
      speakers:
        - name: Carrie Maynard
          title: VP of Integrated Marketing
          company: GitLab
          image:
            src:  /nuxt-images/events/world-tour/speakers/Carrie Maynard.jpeg
          biography:
            text: |
              Carrie Maynard is currently the Vice-President, Integrated Marketing at GitLab. She leads the team that develops the strategy and execution of demand generation programs to deliver leads and revenue to the business.  As a builder of programs, teams and brands, Carrie is focused on the critical intersection of data-driven marketing and the art of storytelling. 

              With over 20+ years of experience in technology, Carrie’s curiosity and love of problem solving has led her to be a part of several transformational opportunities and inflection points in the industry. These include the early evolution of social media and influencers in PR; the mix of art and science in marketing automation; and the evolution of SaaS to cloud and new ways of collaboration.

              Prior to GitLab, Carrie held progressive roles at Google, Microsoft, PwC, Eloqua and a range of small and large organizations in Enterprise IT and semiconductors. As a result, she has a deep skill set focused on growth in demand generation, industry marketing, digital, field and partner marketing, in addition to PR and analyst relations.
      description:
        title: 'Welcome Keynote: GitLab DevSecOps World Tour - The Evolution of DevSecOps'
        text: |
          Join us at the GitLab DevSecOps World Tour in New York City where we’ll be showcasing a comprehensive overview of the evolving DevSecOps landscape. Learn how Artificial Intelligence (AI) is set to impact all teams across the SDLC and how we envision organizations confidently securing their software supply chain while staying laser focused on delivering business value.
    - time: 10:30 am
      name: Next up for The DevSecOps Platform
      speakers:
        - name: Omar Fernandez
          title: Director of Product, Fulfillment
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Omar-Fernandez.jpg
          biography:
            text: Omar is Director of Product Management at GitLab, with a focus on Product Monetization initiatives. With over a decade working in consulting and product management roles, Omar is passionate about solving business problems with technology. Prior to GitLab, Omar spend time both at startups and large companies such as Google and Accenture. 
        - name: Josh Lambert
          title: Director of Product Management, Enablement
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Josh Lambert.png
          biography:
            text: Josh's passion with computers started in elementary school when his parents brought an IBM PCjr home along with a book on BASIC. In his teenage years he spent time working on a variety of coding projects and assembling his own computers. He developed a love for product management to help solve problems with technology, and has been working in the DevSecOps space with GitLab for over 6 years. When not spending time with his family or working on technology, he tries to get out skiing and playing tennis.
      description:
          text: |
            Teams are facing more pressure than ever to deliver software faster — and software development processes are undergoing transformative changes. With recent AI innovations, we see teams developing, securing and deploying software very differently. Join us to hear how our vision for GitLab is set to change how teams function across the entire SDLC, followed by a deep dive into our latest innovations where you will learn how to keep security at the forefront powered by intelligent automations.
    - time: 11:15 am
      name: Break
    - time: 11:30 am
      name: In discussion with Ram Kothur, Ally
      speakers:
        - name: Lee Faus
          title: Global Field CTO
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Lee Faus.png
          biography:
            text: |
              Lee has worked with Senior Executives at Fortune 100 and Global 2000 companies for the past 10 years as a trusted advisor around Cloud Adoption, Agile Methodologies, DevSecOps and Engineering Best Practices.  He leverages his experience as an educator to bring complex concepts into a business forum where executives gain valuable advice that can provide immediate impact to their business.  Lee has given numerous keynotes at conferences internationally, speaking from his experience working across different verticals and vocalizing the challenges of running highly performant engineering teams.  Lee brings work experience from Borland, Compuware, Red Hat, Alfresco, GitHub, and GitLab and marries that with his consulting experiences at State Farm, Pfizer, Travelport, WellsFargo and Nokia to leverage lessons learned from other executives.  While Lee loves to talk about technology, he is also very passionate about measuring business outcomes and ensuring the efficiency of engineering teams while meeting customer demands.
        - name: Ram Kothur
          title: Director, Enterprise DevOps and Cloud Engineering
          company: Ally
          image:
            src: /nuxt-images/events/world-tour/speakers/Ram Kothur.jpg
            position: bottom
          biography:
            text: |
              Ram Kothur is recognized in the field of DevSecOps, with 20+ years of experience in IT development  and delivery as Director, DevSecOps and Cloud Engineering at Ally Financial. As a Director at Ally, Ram has a strong track record of implementing secure and efficient development practices, and he is passionate about integrating security into the entire software development lifecycle.

              With a deep understanding of the challenges and opportunities within the DevSecOps landscape, Ram is poised to provide invaluable insights and practical strategies for achieving a seamless and secure DevSecOps integration and resolving challenges with DevSecOps transformation.
      description:
        text: Join us for this fireside chat and hear from Ally on how they successfully transformed their DevOps processes. You will gain insights on how they approached roadblocks and challenges which in turn helped them to emerge victorious in their DevSecOps journey. You will also learn how they effectively implemented DevSecOps best practices into their process to help their teams work smarter and deliver software faster. In this unique opportunity we also dig deeper into how leading organizations respond to rapidly evolving business needs and economic conditions, with minimal impact to their delivery velocity.
    - time: 12:15 pm
      name: Lunch & Networking
    - time: 1:30 pm
      name: |
        **Workshop:** How to optimize your DevSecOps workflow by focusing on Value Streams
      speakers:
        - name: Ancil McBarnett
          title: Senior Manager, Enterprise Solutions Architecture, East
          company: GitLab
          biography:
            text: |
              Ancil McBarnett is a Senior Manager for Solutions Architects in the Northeast and Canadian Enterprise regions at GitLab, having worked in Solutions Engineering, Architecture and Consulting for 28 years, where he initially began his career as an Oracle consultant and DBA in the Caribbean and Latin America. He originates from Trinidad and Tobago.

              In a past life, he was the Architect Manager  and chief architect for the Pennsylvania Justice Network (PA JNET) , a project initiated by Tom Ridge, the first Homeland Secretary.  So when you drive through PA, and you get a speeding ticket, you can thank him for the high cost.

              He has assisted large corporations including financial firms, architect and revolutionize the way they provision infrastructure, network, and deploy applications at scale, in a self serviced, automated and secured fashion.  This includes hosting and leading roundtables with senior executives, to understand the true impact of the changes brought on by this digital and cloud  transformation, has had on the people and processes to drive business outcomes.
          image:
            src: /nuxt-images/events/world-tour/speakers/Ancil McBarnett.jpg
        - name: David Astor
          title: Solutions Architect Manager
          company: GitLab
          biography:
            text: |
              David Astor is a Solutions Architect Manager with GitLab.  He’s worked with numerous organizations of all sizes to assist them in simplifying their DevOps workflows. He has a great passion for learning new things and loves getting to the “wow!” moment with the folks he helps.

              He’s a listener, a story teller, an agilist, a part time runner, an avid college football fan and loyal husband and friend. He’s a father to an incredible daughter and general purveyor of all manner of useless trivia.

              He’s a huge proponent of breaking down walls and getting people to work together. Oh, and dad jokes.
          image:
            src: /nuxt-images/events/world-tour/speakers/David Astor.png
        - name: Reshmi Krishna
          title: Director, Enterprise Solutions Architecture
          company: GitLab
          biography:
            text: Reshmi Krishna leads Enterprise Solutions Architecture(Americas) for GitLab. Reshmi has extensive experience in being an engineer and solution architect for over 2 decades. Reshmi leads with empathy, and loves to dive deep into challenges. She has expertise in areas such as cloud-native technologies, digital transformation, DevSecOps, value stream mapping. Outside of work, Reshmi loves to represent solutions architecture in various public speaking engagements promoting and coaching folks along the way. Reshmi has recently started writing her own blogs encouraging folks to become a solutions architect with the hopes that one day, she will no longer be a minority in the field.
          image:
            src: /nuxt-images/events/world-tour/speakers/Reshmi Krishna.jpg
        - name: Ron Koster
          title: Solutions Architect
          company: GitLab
          biography:
            text: Located in the greater Boston area, Ron Koster joined GitLab in 2019 as a solution architect. Prior to GitLab, Ron operated in both pre and post sales capacity supporting DevOps for Database as well as change management for ERP systems technologies.
          image: 
            src: /nuxt-images/events/world-tour/speakers/Ron Koster.jpeg
      description:
        text: |
          If your team is looking to optimize your DevOps practices, join the DevOps Value Stream experts for valuable insights and actionable strategies. This interactive workshop will focus on discovery, identification and removal of blockers to drive visibility and continuous improvement. Dive deeper into Value Stream Assessments (VSA) and uncover areas of improvement in the SDLC. You will learn how you can accelerate value streams for faster business value realization. This interactive exercise will also provide practical experience for you to apply these VSA methodologies to your teams to identify key areas for improvement. At the end of this session, you will have a solid understanding of VSA principles, practical tools and techniques to set you ahead on the path to improving your DevOps processes.
    - time: 3:00 pm
      name: Break
    - time: 3:15 pm
      name: |
        Partner Spotlight: BoxBoat & Red Hat
      speakers:
        - name: Toure	Dunnon
          title: Enterprise Architect
          company: 'BoxBoat, an IBM Company'
          image:
            src: /nuxt-images/events/world-tour/speakers/Toure Dunnon.jpg
            contain: false
          biography:
            text: |
              Touré Dunnon is a highly skilled and experienced engineer with 20+ years in system, software, and platform engineering. He has a proven track record of success in designing, developing, and implementing complex systems and platforms. He is a passionate advocate for technology and its potential to improve the world. 

              His key accomplishments include designing and developing cloud-based platforms that save companies millions annually while improving customer satisfaction by 20%. He also leads a team of engineers who develop new platforms that increase client revenue by 15%. 

        - name: Bill Gagliard
          title: 'ISV Ecosystem Manager'
          company: 'Red Hat'
          image:
            src: /nuxt-images/events/world-tour/speakers/william-gagliard.jpg
            contain: false
          biography:
            text: |
              As a Partner Alliance Manager, Bill Gagliard has two decades of experience in the tech industry. He works on co-selling and creating joint solutions with GitLab, IBM, and Red Hat. He helps partners leverage the Red Hat Partner Organization’s modern delivery methods to support a hybrid cloud strategy. This strategy enables partners to build and run applications on any cloud using Red Hat Enterprise Linux, Ansible Automation, and OpenShift. It also incorporates the best practices of continuous integration and deployment (CI/CD) to help customers thrive in the cloud. Before joining Red Hat, Bill sold cloud services to large enterprises in the Central U.S. for Microsoft (10 years) and AWS (4 years).
      description:
        text: |
          Join us and hear from BoxBoat & RedHat on how they are uniquely positioned to power cloud innovation and deliver customer value alongside business transformation. Learn how, with the combined strength of GitLab, Box Boat & RedHat, customers are achieving their business goals with an accelerated pace of innovation and faster time to market.
    - time: 3:40 pm
      name: |
        **Roundtable discussion:** The human element of DevSecOps
      speakers:
        - name: David Astor
          title: Solutions Architect Manager
          company: GitLab
          biography:
            text: |
              David Astor is a Solutions Architect Manager with GitLab. He’s worked with numerous organizations of all sizes to assist them in simplifying their DevOps workflows. He has a great passion for learning new things and loves getting to the “wow!” moment with the folks he helps.

              He’s a listener, a story teller, an agilist, a part time runner, an avid college football fan and loyal husband and friend. He’s a father to an incredible daughter and general purveyor of all manner of useless trivia.

              He’s a huge proponent of breaking down walls and getting people to work together. Oh, and dad jokes.
          image:
            src: /nuxt-images/events/world-tour/speakers/David Astor.png
      description:
          text: |
            ‘Technology isn’t the hard problem anymore, culture seems to be the chief problem now.’- Is this a statement that you have come across?

            We invite you to discuss with leaders the role and impact of people on DevOps processes. Dig deeper into why communication, collaboration and empathy play a critical role in effective DevOps strategies. What are the strategies that work while introducing DevOps into businesses. Does it work if you find the core problem the organization faces and start there? Is education on agile a great place to start when thinking about building a DevOps culture? Join us and weigh in on the best practices to foster teamwork, promote accountability and  develop a human-centered design for DevOps processes.

    - time: 4:10 pm
      name: Closing Remarks
      speakers:
        - name: Ginny Reib
          title: Senior Manager AMER Field Marketing
          company: GitLab
          image: 
            src: /nuxt-images/events/world-tour/speakers/Ginny Reib.jpg
    - time: 4:30 pm
      name: Snacks + Networking
  sponsors:
    - img: /nuxt-images/events/world-tour/sponsors/redhad-boxboat.png
      alt: DevOps 1
  form:
    multi_step: true
    header: Register for DevSecOps World Tour in New York
    confirmation:
      - Thank you for registering!
      - Please check your email for a confirmation email.
    form_id: '3703'
  conduct_banner:
    header: Code of conduct
    text: GitLab is committed to providing a safe and welcoming experience for every attendee at all of our events whether they are virtual or onsite. Please review our [code of conduct](/company/culture/ecoc/) to ensure DevSecOps World Tour is a friendly, inclusive, and comfortable environment for all participants.
