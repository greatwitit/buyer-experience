---
  title: Professional Services
  description: Need help installing, upgrading, or migrating to GitLab? We can help with migration services or simply upgrading your current instance.
  hero:
      title: GitLab Professional Services
      subtitle: With Professional Services help, customers will be able accelerate their GitLab adoption journey with implementation, migration, advisory, acceleration, and education services designed to maximize the value GitLab can bring to their business. 
      modal_src: https://www.youtube.com/embed/FmimIGNR2S4?autoplay=1
      aos_animation: fade-down
      aos_duration: 500
      img_animation: zoom-out-left
      img_animation_duration: 1600
      primary_btn:
        url: "#contactform"
        text: Request Service
        data_ga_name: request service
        data_ga_location: header
# Comment out Demo button until we have a new video
#      secondary_btn:
#        modal: true
#        data_ga_name: watch a demo
#        data_ga_location: header
#        text: Watch a demo
#        variant: tertiary
#        icon:
#          name: play
#          variant: product
      image:
        image_url: /nuxt-images/services/hero.jpeg
        alt: "Image: GitLab Professional Services"
        bordered: true
 
  side_menu:
    anchors:
      text: On this page
      data:
        - text: Implementation Services
          href: "#implementation-services"
          data_ga_name: implementation services
          data_ga_location: side-navigation
        - text: Migration Services
          href: "#migration-services"
          data_ga_name: migration services
          data_ga_location: side-navigation
        - text: Advisory Services
          href: "#advisory-services"
          data_ga_name: advisory services
          data_ga_location: side-navigation
        - text: Acceleration Services
          href: "#acceleration-services"
          data_ga_name: Acceleration services
          data_ga_location: side-navigation
        - text: Education Services
          href: "#education-services"
          data_ga_name: education services
          data_ga_location: side-navigation
    hyperlinks:
      data:
        - text: Request Services
          href: "#contactform"
          variant: primary
          data_ga_name: request services
          data_ga_location: side-navigation
        # Comment out button until we have a new video
        # - text: Watch a demo
        #   modal: true
        #   icon:
        #     name: play
        #     variant: product
        #   variant: tertiary
        #   data_ga_name: learn-about-ultimate
        #   data_ga_location: side-navigation
  blog_copy:
    block:
      - header: Implementation Services
        id: implementation-services
        sections:
          - type: copy
            text: |
              Deploy - Configure - Stabilize

              Whether your team chose GitLab to accelerate your DevSecOps transformation, improve collaboration between all the roles in your organization, or facilitate an innersourcing initiative - the challenge is to get to that desired end state as fast as possible. We have a team of professionals that will help you spin up a stable GitLab instance that will meet your business needs.
              
          - type: card
            title: Rapid Results Consulting
            text: |
              The GitLab Rapid Results Consulting package helps you quickly implement your GitLab solution and enable your organization to take advantage of your GitLab purchase quickly.

               - [Rapid Results, SaaS](https://drive.google.com/file/d/1dtCtZjCgolbO0lZ8BzTfPG_BNC6Tv078/view?usp=drive_link)
               - [Rapid Results, Self-Managed](https://drive.google.com/file/d/18LSkP5ZqJDkzLO2558W_70hS3KsjfjJe/view?usp=drive_link)

            # link:
            #   text: Learn more
            #   href: /services/rapid-results/
            #   data_ga_name: rapid results consulting
            #   data_ga_location: body
          - type: card
            title: Implementation Consulting
            text: |
              For customers who want to quickly deploy a stable GitLab instance for up to 1000 users, and learn how to maintain it, we recommend an implementation engagement from our team of Professional Services Engineers.

               - [Implementation QuickStart](https://drive.google.com/file/d/1ZQH8S5YJCA228ZypIXL5cs-8ccynCScP/view?usp=drive_link)

            # link:
            #   text: Reference Architectures
            #   href: https://docs.gitlab.com/ee/administration/reference_architectures/
            #   data_ga_name: dedicated implementation planning
            #   data_ga_location: body
          
      - header: Migration Services
        id: migration-services
        sections:
          - type: copy
            text: |
              Users - Data - Workflows

              Your data is valuable and making it readily available for immediate continuity is indispensable. Our migration services will accelerate and smoothen your transition to GitLab so you can focus on your business.
          - type: card
            column_size: 6
            title: Migration Consulting
            text: |
              The GitLab Migration QuickStart package enables most companies to migrate their data from previous source code management systems.

              - [Migration QuickStart](https://drive.google.com/file/d/1dPs65e23bzlBQ_DxXbAkzZl-6t5OkFzZ/view?usp=drive_link)
        
            # link:
            #   text: Learn more
            #   href: /services/migration/migration-plus/
            #   data_ga_name: migration package
            #   data_ga_location: body
          # - type: card
          #   column_size: 6
          #   title: Custom-Scoped Migration
          #   text: |
          #     For larger customers who have more users and repositories and may have more complex data migration requirements including multiple source systems.

            # link:
            #   text: Learn more
            #   href: /services/migration/enterprise/
            #   data_ga_name: custom scoped migration
            #   data_ga_location: body
      - header: Advisory Services
        id: advisory-services
        sections:
          - type: copy
            text: |
              Innovate - Experiment - Pilot

              The GitLab platform provides an opportunity to transform the way your business operates. But making that transition from your legacy way of working to a more efficient and effective value stream can be challenging. Engage with GitLab experts to solve specific challenges or keep a GitLab Expert on retainer to help guide you on your journey.
          - type: card
            title: DevSecOps Workshop
            text: |
              The DevSecOps workshops offering allows for GitLab engineers to show off GitLab Ultimate's vast security features and demonstrate security design patterns as well as provide strategic guidance to create a roadmap to kick start your DevSecOps journey with GitLab. 

              - [DevSecOps Workshop](https://drive.google.com/file/d/1mZm_DiwPdtssFqBolrDqPooaH6kA5Y5u/view?usp=drive_link)
            # link:
            #   text: Learn more
            #   href: /services/advisory/advisory-workshop
            #   data_ga_name: advisory workshop
            #   data_ga_location: body

          - type: card
            title: Health Check
            text: |
              Let's evaluate your deployment architecture to provide recommendations on how to optimize for performance, stability, and availability.

              - [HealthCheck, Self-Managed](https://drive.google.com/file/d/1OWZdw44MMaYLyrvxGo96vYuzz5wTXeaq/view?usp=drive_link)
            # link:
            #   text: Learn more
            #   href: /services/implementation/health-check/
            #   data_ga_name: health check
            #   data_ga_location: body
          
            # link:
            #   text: Learn more
            #   href: /services/advisory/expert-services/
            #   data_ga_name: advisory workshop
            #   data_ga_location: body
      - header: Acceleration Services
        id: acceleration-services
        sections:
          - type: copy
            text: |
              Enable

              The GitLab platform provides unsurpassed configurability in order to accommodate the most complex and challenging enterprise environments. Utilizing all of its capabilities can be challenging without having the right level of assistance and ongoing expertise. Enabling your organization at all levels is critical to ensuring your success at all levels.
          - type: card
            title: Dedicated Engineer
            list_style: number
            text: |
              We offer dedicated engineer services providing dedicated engineers for three set durations, 3, 6 and 12 months. During these engagements a PS Engineer will work with you full time, focusing on building solutions to business problems using GitLab. As the engagements get longer, the prices are discounted. Please see the below engagement SOWs for pricing and specific activities for this engagement.

              - [Dedicated Engineer](https://drive.google.com/file/d/1715uZ7sAu00PK7-5jaMsfWYzp6GZcBT7/view?usp=drive_link)
              - [Dedicated Engineer with Security Clearance](https://drive.google.com/file/d/1YhcQ92828H120EpQJOrY3Mq-4prdr1XU/view?usp=drive_link)
          - type: card
            title: Dedicated Program Manager
            list_style: number
            text: |
              We offer dedicated program manager services providing dedicated engineers for three set durations, 3, 6 and 12 months. During these engagements a PS Engineer will work with you full time, focusing on building solutions to business problems using GitLab. As the engagements get longer, the prices are discounted. Please see the below engagement SOWs for pricing and specific activities for this engagement.

              - [Dedicated Program Management](https://drive.google.com/file/d/1BqR17Bd5JGry8SdxAhrX_ykAsgBpxvqZ/view?usp=drive_link)
          - type: card
            title: Expert Services
            text: |
              For customers who might not know exactly what they need help with at the time of their license purchase/expansion, we can provide guidance on a wide range of topics including best practices, workflows, GitLab implementation, and data migration.

              - [Expert Services](https://drive.google.com/file/d/1Di3vmtkSbzH7RgqCxDrE6iHzHdsN2aVS/view?usp=drive_link)
          - type: card
            title: CI/CD Consulting
            text: |
              For customers who want to modernize the way the deliver software using GitLab CI, we provide a skilled GitLab engineer to help build automated end to end automated CI/CD pipelines customized to your context. 

              - [CI/CD App Modernization](https://drive.google.com/file/d/1ib6-xhja3WJbV_46rU2iDF9I-4I8xo8M/view?usp=drive_link)
          - type: card
            title: Security Consulting
            text: |
              For customers who want to tranform their security process by building in automated scans, defining and enforcing standard compliance requirements and policies, and simplifying the vulnerability management and reporting process, we provide a skilled GitLab engineer to build out your custom security workflow into your GitLab environment.
              - [DevSecOps App Transformation](https://drive.google.com/file/d/1TDJSVO9uvy4NqC6uksQsSc_sSgEcpacV/view?usp=drive_link)
          
             
      - header: Education Services
        id: education-services
        sections:
          - type: copy
            text: |
              Your teams are composed of highly proficient technical experts. With any comprehensive solution, there is a learning curve to consider. Our product specialists—technology professionals themselves—are available to train your teams to become efficient in GitLab and DevSecOps quickly.

              We are working to make all of our educational offerings more widely available by updating our current learning infrastructure to support a broader audience. We will be posting updates to our GitLab Blog as we ready our new platform. Be sure to subscribe to see the latest updates.
          - type: copy
            title: Who is this for?
            text: |
              Anyone looking to cut "under the curve" of learning not only GitLab but Concurrent DevSecOps best practices.
          - type: card
            title: What is included?
            text: |
              GitLab offers a variety of courses delivered at your site or remotely by our experienced GitLab trainers. In addition, we offer specialized training that can be customized even further to meet your team's unique needs.

              Here are the standard courses we offer:

              - [GitLab with Git Essentials Training](/services/education/gitlab-basics/){data-ga-name="gitlab with git basics training" data-ga-location="body"}

              - [GitLab CI/CD Training](/services/education/gitlab-ci/){data-ga-name="gitlab ci/cd training" data-ga-location="body"}

              - [GitLab for Project Managers Training](/services/education/pm/){data-ga-name="gitlab for project managers training" data-ga-location="body"}

              - [GitLab Security Essentials Training](/services/education/security-essentials/){data-ga-name="gitlab security essentials training" data-ga-location="body"}

              - [GitLab DevOps Fundamentals Training](/services/education/devops-fundamentals/){data-ga-name="gitlab devops fundamentals training" data-ga-location="body"}

              - [GitLab System Administration Training](/services/education/admin/){data-ga-name="gitlab system administration training" data-ga-location="body"}

              Here are the standard technical certifications we offer:

              - [GitLab Certified Git Associate](/services/education/){data-ga-name="gitlab certified git associate" data-ga-location="body"}

              - [GitLab Certified CI/CD Associate](/services/education/gitlab-cicd-associate/){data-ga-name="gitlab certified ci/cd associate" data-ga-location="body"}

              - [GitLab Certified Project Management Associate](/services/education/gitlab-project-management-associate/){data-ga-name="gitlab certified project management associate" data-ga-location="body"}

              - [GitLab Certified Security Specialist](/services/education/gitlab-security-specialist/){data-ga-name="gitlab certified security specialist" data-ga-location="body"}

              - [GitLab Certified DevOps Professional](/services/education/gitlab-certified-devops-pro/){data-ga-name="gitlab certified ci/cd specialist" data-ga-location="body"}              


              For organizations that need to scale training to a larger audience, we also offer a Train the Trainer option.

              For a complete list of Professional Services offerings visit the [Full Catalog page](https://about.gitlab.com/services/catalog/).
  form_block:
    id: 'contactform'
    header: Interested in GitLab Professional Services? Get in touch.
    form:
      form_id: 1476
      datalayer: services
