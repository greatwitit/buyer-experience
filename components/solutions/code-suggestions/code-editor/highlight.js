import { CODE_HIGHLIGHTING } from '~/common/constants.ts';

/*
    Receives a character to be classified and returns a classed span tag
*/
const highlight = (char, language = 'javascript', isComment = false) => {
  if (isComment) {
    return `<span class="comment">${char}</span>`;
  }
  // There are language-specific keywords to be considered. The default is javascript
  if (CODE_HIGHLIGHTING[language].keywords.includes(char)) {
    return `<span class="keyword">${char}</span>`;
  }
  if (CODE_HIGHLIGHTING.operators.includes(char)) {
    return `<span class="operator">${char}</span>`;
  }
  if (CODE_HIGHLIGHTING.brackets.includes(char)) {
    return `<span class="bracket">${char}</span>`;
  }

  return char;
};

// eslint-disable-next-line import/no-default-export
export default highlight;
