export const NAV_CONTENT_TYPES = Object.freeze({
  NAVIGATION: 'navigation',
  FOOTER: 'footer',
});

export const CONTENT_TYPES = Object.freeze({
  PAGE: 'page',
  CODE_SUGGESTIONS_IDE: 'codeSuggestionsIde',
  NEXT_STEPS: 'nextSteps',
  EVENT: 'event',
});
